package net.toften.jlips2.test;

import java.sql.ResultSet;

import net.toften.jlips2.db.data.connection.DbConnectionManager;

public class DbConnectionManagerTest extends AbstractDbWithConnectionFactoryTest {
	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionManager.DbConnectionManager(ConnectionFactory)'
	 */
	public void testDbConnectionManager() {
		DbConnectionManager dbc;
		
		dbc = new DbConnectionManager(dbcf);
		assertNotNull(dbc);
		
		try {
			dbc = new DbConnectionManager(null);
			fail();
		} catch (NullPointerException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionManager.setStatement(String)'
	 */
	public void testSetStatement() {
		DbConnectionManager dbc;
		
		dbc = new DbConnectionManager(dbcf);
		
		// Set an empty statement
		dbc.setStatement("");
		try {
			dbc.execute();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		} finally {
			try {
				dbc.release();
			} catch (Exception e) {
				fail();
			}
		}
		
		// Set an null statement
		dbc.setStatement(null);
		try {
			dbc.execute();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		} finally {
			try {
				dbc.release();
			} catch (Exception e) {
				fail();
			}
		}
		
		// Set a real statement
		dbc.setStatement("SELECT * FROM person");
		try {
			dbc.execute();
		} catch (Exception e) {
			fail();
		} finally {
			try {
				dbc.release();
			} catch (Exception e) {
				fail();
			}
		}

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionManager.release()'
	 */
	public void testRelease() {
		// Tested in testSetStatement
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionManager.execute()'
	 */
	public void testExecute() {
		// Tested in testSetStatement
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionManager.getResultSet()'
	 */
	public void testGetResultSet() {
		DbConnectionManager dbc;
		dbc = new DbConnectionManager(dbcf);
		
		ResultSet rs = null;
		try {
			rs = dbc.getResultSet();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		}catch (Exception e) {
			fail();
		}
		
		// Set a real statement
		dbc.setStatement("SELECT * FROM person");
		try {
			dbc.execute();
		} catch (Exception e) {
			fail();
		}
		
		try {
			rs = dbc.getResultSet();
		} catch (Exception e) {
			fail();
		}
		
		// Test resultset
		try {
			while (rs.next()) {
				assertEquals("Thomas", rs.getString("FIRSTNAME"));
				assertEquals("Larsen", rs.getString("LASTNAME"));
				assertEquals(37, rs.getInt("AGE"));
			}
		} catch (Exception e) {
			fail();
		} finally {
			try {
				dbc.release();
			} catch (Exception e) {
				fail();
			}
		}

	}

}
