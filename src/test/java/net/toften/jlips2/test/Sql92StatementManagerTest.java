package net.toften.jlips2.test;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory;
import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.db.data.statement.Sql92StatementFactory;
import net.toften.jlips2.db.data.statement.Sql92StatementManager;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.db.HsqlStatementFactory;
import net.toften.jlips2.test.db.HsqlStatementManager;
import net.toften.jlips2.test.stubs.TableInfoStub;

public class Sql92StatementManagerTest extends AbstractDbTest {
	Sql92StatementFactory s92sf;
	DbPrimarykeyFactory pkf;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		s92sf = new Sql92StatementFactory();
		s92sf.init(p);
		
		// Primary key factory is used to create primary keys for the SQL statements
		pkf = new DbPrimarykeyFactory();
		pkf.init(p);
	}

	
	
	Sql92StatementManager sm; // Used in tests
	
	static TableInfo pti = TableInfoStub.getPersonTableInfo();
	static TableInfo jti = TableInfoStub.getJobTableInfo();
	
	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.getConnectionManager()'
	 */
	public void testGetConnectionManager() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));

		assertNotNull(sm.createConnectionManager());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.createUpdateStatement()'
	 */
	public void testCreateUpdateStatement() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		assertEquals("update person", sm.createUpdateStatement());
		
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(23)));
		assertEquals("update person where id=23", sm.createUpdateStatement());
		
		sm.addEqualExpression("thomas", pti.columns.get(1));
		assertEquals("update person set firstname='thomas' where id=23", sm.createUpdateStatement());

		sm.addEqualExpression(37, pti.columns.get(3));
		assertEquals("update person set firstname='thomas',age=37 where id=23", sm.createUpdateStatement());

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.createDeleteStatement()'
	 */
	public void testCreateDeleteStatement() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		assertEquals("delete from person" , sm.createDeleteStatement());

		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(23)));
		assertEquals("delete from person where id=23" , sm.createDeleteStatement());

		sm.addEqualExpression("thomas", pti.columns.get(1));
		assertEquals("delete from person where id=23 and firstname='thomas'" , sm.createDeleteStatement());
		sm.addEqualExpression(37, pti.columns.get(3));
		assertEquals("delete from person where id=23 and firstname='thomas' and age=37" , sm.createDeleteStatement());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.createInsertStatement()'
	 */
	public void testCreateInsertStatement() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		assertEquals("insert into person () values ()" , sm.createInsertStatement());
		sm.addEqualExpression("thomas", pti.columns.get(1));
		assertEquals("insert into person (firstname) values ('thomas')" , sm.createInsertStatement());
		sm.addEqualExpression(37, pti.columns.get(3));
		assertEquals("insert into person (firstname,age) values ('thomas',37)" , sm.createInsertStatement());

		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(23)));
		assertEquals("insert into person (firstname,age) values ('thomas',37)" , sm.createInsertStatement());
		
		pti.primarykeyColumn.isAutoGenerate = false;
		assertEquals("insert into person (id,firstname,age) values (23,'thomas',37)" , sm.createInsertStatement());

		pti.primarykeyColumn.isAutoGenerate = true;
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti));
		assertEquals("insert into person (id) values (null)" , sm.createInsertStatement());

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.createSelectStatement()'
	 */
	public void testCreateSelectStatement() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		assertEquals("select * from person", sm.createSelectStatement());
		sm.addTable(pti);
		assertEquals("select * from person", sm.createSelectStatement());
		sm.addTable(jti);
		assertEquals("select * from person,job", sm.createSelectStatement());
		sm.addColumn(jti.columns.get(2));
		assertEquals("select name from person,job", sm.createSelectStatement());
		sm.addColumn(pti.columns.get(3));
		assertEquals("select name,age from person,job", sm.createSelectStatement());
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(23)));
		assertEquals("select name,age from person,job where id=23", sm.createSelectStatement());
		
		// This test is actually a test of the primary key manager, which is also performed
		// in the test for that class
		try {
			pkf.getPrimarykeyManager(pti, Ident.newStringId("unique"));
			fail(); // Primary key id of wrong type
		} catch (IllegalArgumentException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
		sm.addEqualExpression("thomas", pti.columns.get(1));
		assertEquals("select name,age from person,job where id=23 and firstname='thomas'", sm.createSelectStatement());
		
	}

	/*
	 * Test#2 method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.createSelectStatement()'
	 */
	public void testCreateSelectStatement2() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		assertEquals("select * from person", sm.createSelectStatement());
		sm.addEqualExpression("thomas", pti.columns.get(1));
		assertEquals("select * from person where firstname='thomas'", sm.createSelectStatement());

	}

	/*
	 * Test#3 method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.createSelectStatement()'
	 */
	public void testCreateSelectStatement3() {
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		assertEquals("select * from person", sm.createSelectStatement());
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(23)));
		assertEquals("select * from person where id=23", sm.createSelectStatement());

	}

	
	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementManager.Sql92StatementManager(Sql92StatementFactory, String)'
	 */
	public void testSql92StatementManager() {

	}
	
	public void testDoSelect() {
		int lastPk = getLastPrimarykeyValueFromDatabase("person");
		
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(lastPk)));
		assertEquals("select * from person where id=" + lastPk, sm.createSelectStatement());
		
		try {
			ResultSet rs;
			assertNotNull(rs = sm.doSelect());
			
			boolean hasNext;
			assertTrue(hasNext = rs.next());
			while (hasNext) {
				assertEquals("Thomas", rs.getString("firstname"));
				assertEquals("Larsen", rs.getString("lastname"));
				assertEquals(37, rs.getInt("age"));
				
				assertFalse(hasNext = rs.next());
			}
		} catch (Exception e) {
			fail();
		}
		
	}
	
	public void testDoUpdate() {
		int lastPk = getLastPrimarykeyValueFromDatabase("person");
		
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(lastPk)));
		sm.addEqualExpression("Thomas Bahn", pti.columns.get(1));
		assertEquals("update person set firstname='Thomas Bahn' where id=" + lastPk, sm.createUpdateStatement());

		try {
			sm.doUpdate();
		} catch (Exception e) {
			fail();
		}
		

		// Check that the record has been updated
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(lastPk)));
		assertEquals("select * from person where id=" + lastPk, sm.createSelectStatement());

		try {
			ResultSet rs;
			assertNotNull(rs = sm.doSelect());
			
			boolean hasNext;
			assertTrue(hasNext = rs.next());
			while (hasNext) {
				assertEquals("Thomas Bahn", rs.getString("firstname"));
				assertEquals("Larsen", rs.getString("lastname"));
				assertEquals(37, rs.getInt("age"));
				
				assertFalse(hasNext = rs.next());
			}
		} catch (Exception e) {
			fail();
		}
				
	}
	
	public void testdoDelete() {
		
	}
	
	public void testDoInsert() {
		HsqlStatementFactory hsf = new HsqlStatementFactory();
		PrimaryKeyManager pkm;
		int lastPk = getLastPrimarykeyValueFromDatabase("person");
		
		
		try {
			hsf.init(p);
		} catch (Exception e) {
			fail();
		}
		
		assertNotNull(sm = new HsqlStatementManager(hsf, pti));
		assertEquals("insert into person () values ();call identity()" , sm.createInsertStatement());
		
		assertNotNull(pkm = pkf.getPrimarykeyManager(pti));
		sm.setPrimarykey(pkm);
		assertEquals("insert into person (id) values (null);call identity()" , sm.createInsertStatement());
		try {
			assertNotNull(pkm = sm.doInsert());
		} catch (SQLException e) {
			fail();
		}
		assertEquals(lastPk + 1, pkm.getValue());
		
		assertNotNull(sm = new HsqlStatementManager(hsf, pti));
		assertNotNull(pkm = pkf.getPrimarykeyManager(pti));
		sm.setPrimarykey(pkm);
		sm.addEqualExpression(15, pti.columns.get(3));
		assertEquals("insert into person (age) values (15);call identity()" , sm.createInsertStatement());
		try {
			assertNotNull(pkm = sm.doInsert());
		} catch (SQLException e) {
			fail();
		}
		
		// Check that the record has been updated
		assertEquals(lastPk + 2, pkm.getValue());
		assertNotNull(sm = new Sql92StatementManager(s92sf, pti));
		
		sm.setPrimarykey(pkf.getPrimarykeyManager(pti, Ident.newIntegerId(lastPk + 2)));
		assertEquals("select * from person where id=" + (lastPk + 2), sm.createSelectStatement());

		try {
			ResultSet rs;
			assertNotNull(rs = sm.doSelect());
			
			boolean hasNext;
			assertTrue(hasNext = rs.next());
			while (hasNext) {
				assertEquals(15, rs.getInt("age"));
				
				assertFalse(hasNext = rs.next());
			}
		} catch (Exception e) {
			fail();
		}
		
		
	}

}
