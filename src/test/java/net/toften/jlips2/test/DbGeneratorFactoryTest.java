package net.toften.jlips2.test;


import java.sql.Types;

import net.toften.jlips2.db.data.primarykey.generator.DbGeneratorFactory;
import net.toften.jlips2.db.data.primarykey.generator.GeneratorFactory;
import net.toften.jlips2.db.data.primarykey.generator.GeneratorManager;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.TableInfoStub;

public class DbGeneratorFactoryTest extends AbstractFactoryTest {
	TableInfo t = null;
	DbGeneratorFactory gf = new DbGeneratorFactory();
	
	protected void setUp() throws Exception {
		super.setUp();
		
		t = TableInfoStub.getPersonTableInfo();
	}
	
	public void testInit() {
		try {
			assertEquals(true, gf.init(p));
		} catch (Exception e) {
			fail();
		}
		assertEquals(true, gf instanceof GeneratorFactory);
	}
	
	public void testGetGeneratorManager() {
		try {
			gf.getGeneratorManager(null);
			fail();
		} catch (IllegalArgumentException e) {
			// expected exception
		}
		
		t.primarykeyColumn.lastPrimarykeyValue = null;
		try {
			gf.getGeneratorManager(t);
		} catch (IllegalStateException e) {
			// expected exception
		}

		t.primarykeyColumn.sqlType = Types.VARCHAR;
		try {
			gf.getGeneratorManager(t);
		} catch (IllegalStateException e) {
			// expected exception
		}
		
		t.primarykeyColumn.sqlType = Types.INTEGER;
		t.primarykeyColumn.lastPrimarykeyValue = new Integer(2);
		
		GeneratorManager cm1 = gf.getGeneratorManager(t);
		GeneratorManager cm2 = gf.getGeneratorManager(t);
		
		assertEquals(cm1, cm2);
	}

}
