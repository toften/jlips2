package net.toften.jlips2.test;

import java.util.Properties;

import junit.framework.TestCase;
import net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory;
import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.db.data.primarykey.generator.GeneratorFactory;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.PropertiesStub;
import net.toften.jlips2.test.stubs.TableInfoStub;

public class DbPrimarykeyFactoryTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory.init(Properties)'
	 */
	public final void testInit() {
		Properties prop = PropertiesStub.getPropertiesWithEntityMap();
		
		DbPrimarykeyFactory pf = new DbPrimarykeyFactory();
		try {
			assertTrue(pf.init(prop));
		} catch (Exception e) {
			fail();
		}
		assertNull(pf.findGeneratorFactory());
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory.getPrimarykeyManager(TableInfo)'
	 */
	public final void testGetPrimarykeyManagerTableInfo() {
		Properties prop = PropertiesStub.getPropertiesWithEntityMap();
		TableInfo p1 = TableInfoStub.getPersonTableInfo();
		
		DbPrimarykeyFactory pf1 = new DbPrimarykeyFactory();

		prop.put(GeneratorFactory.PROP_GENERATORFACTORY, "net.toften.jlips2.db.data.primarykey.generator.DbGeneratorFactory");
		try {
			pf1.init(prop);
		} catch (Exception e1) {
			fail();
		}
		
		PrimaryKeyManager pm1 = pf1.getPrimarykeyManager(p1);
		checkManager(pm1, p1);
		
		
		TableInfo p2 = TableInfoStub.getPersonTableInfo();
		p2.primarykeyColumn.isAutoGenerate = false;

		DbPrimarykeyFactory pf2 = new DbPrimarykeyFactory();

		try {
			pf2.init(prop);
		} catch (Exception e1) {
			fail();
		}

		try {
			pf2.getPrimarykeyManager(p2);
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
	}
	
	private void checkManager(PrimaryKeyManager pm, TableInfo p) {
		assertNotNull(pm);
		assertTrue(pm.isAutoGenerate());
		assertTrue(pm.isNumeric());
		assertNull(pm.getValue());
		assertEquals(p.primarykeyColumn.getName(), pm.getPrimarykeyName());
		assertEquals(p.primarykeyColumn, pm.getPrimarykeyColumn());
		assertNull(pm.getStatementString());
		assertNull(pm.toString());
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory.getPrimarykeyManager(TableInfo, ID)'
	 */
	public final void testGetPrimarykeyManagerTableInfoID() {
		// TODO Auto-generated method stub

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory.findGeneratorFactory(TableInfo)'
	 */
	public final void testFindGeneratorFactory() {
		Properties prop = PropertiesStub.getPropertiesWithEntityMap();
		
		DbPrimarykeyFactory pf = new DbPrimarykeyFactory();

		prop.put(GeneratorFactory.PROP_GENERATORFACTORY, "net.toften.jlips2.db.data.primarykey.generator.DbGeneratorFactory");
		try {
			pf.init(prop);
		} catch (Exception e) {
			fail();
		}
		assertNotNull(pf.findGeneratorFactory());
	}

}
