package net.toften.jlips2.test;

import java.sql.Types;

import junit.framework.TestCase;
import net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory;
import net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager;
import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.TableInfoStub;

public class DbPrimarykeyManagerTest extends TestCase {
	/**
	 * p1:
	 *     Autogenerate is true
	 *     Primary key is INTEGER
	 *     
	 * p2:
	 *     Autogenerate is true
	 *     Primary key is VARCHAR
	 */
	TableInfo p1 = TableInfoStub.getPersonTableInfo();
	TableInfo p2 = TableInfoStub.getPersonTableInfo();
	
	{
		p1 = TableInfoStub.getPersonTableInfo();
		
		p2 = TableInfoStub.getPersonTableInfo();
		p2.primarykeyColumn.sqlType = Types.VARCHAR;		
	}
	
	DbPrimarykeyFactory pkf = new DbPrimarykeyFactory();
	
	public DbPrimarykeyManagerTest()
	{
		try {
			pkf.init(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	ID id2 = Ident.newStringId("10");
	
	PrimaryKeyManager pkm1 = new DbPrimarykeyManager(pkf, p1);
	PrimaryKeyManager pkm2 = new DbPrimarykeyManager(pkf, p2, (Ident)id2);
	
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.toString()'
	 */
	public void testToString() {
		assertNull(pkm1.toString());
		assertEquals("10", pkm2.toString());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.isNumeric()'
	 */
	public void testIsNumeric() {
		assertTrue(pkm1.isNumeric());
		assertFalse(pkm2.isNumeric());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.DbPrimarykeyManager(TableInfo)'
	 */
	public void testDbPrimarykeyManagerTableInfo() {
		TableInfo p = TableInfoStub.getPersonTableInfo();
		
		new DbPrimarykeyManager(pkf, p);
		
		p.primarykeyColumn.isAutoGenerate = false;	
		try {
			new DbPrimarykeyManager(pkf, p);
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.DbPrimarykeyManager(TableInfo, Ident)'
	 */
	public void testDbPrimarykeyManagerTableInfoIdent() {
		TableInfo p = TableInfoStub.getPersonTableInfo();
		ID intId = Ident.newIntegerId(3);
		ID strId = Ident.newStringId("test");
		
		PrimaryKeyManager pkm = null;
		
		try {
			new DbPrimarykeyManager(pkf, p, null);
			fail();
		} catch (NullPointerException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}

		try {
			new DbPrimarykeyManager(pkf, p, (Ident)strId);
			fail();
		} catch (IllegalArgumentException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
		pkm = new DbPrimarykeyManager(pkf, p, (Ident)intId);
		assertEquals(3, ((Integer)pkm.getValue()).intValue());
		
		p.primarykeyColumn.sqlType = Types.VARCHAR;
		try {
			new DbPrimarykeyManager(pkf, p, (Ident)strId);
		} catch (Exception e) {
			fail();
		}	

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.getPrimarykeyName()'
	 */
	public void testGetPrimarykeyName() {
		assertEquals(p1.primarykeyColumn.getName(), pkm1.getPrimarykeyName());		

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.isAutoGenerate()'
	 */
	public void testIsAutoGenerate() {
		assertTrue(pkm1.isAutoGenerate());

		/*
		 * TODO Test for a non-autogenerate
		 */
		TableInfo p = TableInfoStub.getPersonTableInfo();
		
		assertNotNull(new DbPrimarykeyManager(pkf, p));
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.processStatementAfterInsert(Statement)'
	 */
	public void testProcessStatementAfterInsert() {
		// TODO
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.getPrimarykeyColumn()'
	 */
	public void testGetPrimarykeyColumn() {
		assertEquals(p1.primarykeyColumn, pkm1.getPrimarykeyColumn());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.DbPrimarykeyManager.getValue()'
	 */
	public void testGetValue() {
		assertNull(pkm1.getValue());
		assertEquals(((Ident)id2).getValue(), pkm2.getValue());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.primarykey.AbstractPrimarykeyManager.getStatementString()'
	 */
	public void testGetStatementString() {
		assertNull(pkm1.getStatementString());
		assertEquals("'10'", pkm2.getStatementString());
		
		TableInfo p = TableInfoStub.getPersonTableInfo();
		ID id = Ident.newIntegerId(4);
		
		PrimaryKeyManager pkm = new DbPrimarykeyManager(pkf, p, (Ident)id);
		assertEquals("4", pkm.getStatementString());
	}

}
