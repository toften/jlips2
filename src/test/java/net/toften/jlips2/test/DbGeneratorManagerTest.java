package net.toften.jlips2.test;

import java.sql.Types;

import junit.framework.TestCase;
import net.toften.jlips2.db.data.primarykey.generator.DbGeneratorManager;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.TableInfoStub;

public class DbGeneratorManagerTest extends TestCase {
	TableInfo pti = null;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		/*
		 * Set up dummy table (person)
		 */
		
		pti = TableInfoStub.getPersonTableInfo();
		pti.primarykeyColumn.isAutoGenerate = false;
		
	}
	
	public void testGetNewPrimarykeyValue() {
		DbGeneratorManager gm = new DbGeneratorManager(pti);
		
		// Existing primary key value not set
		try {
			gm.getNewPrimarykeyValue();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
		pti.primarykeyColumn.lastPrimarykeyValue = new Integer(2);		
		assertEquals(3, ((Integer)gm.getNewPrimarykeyValue()).intValue());
		assertEquals(3, ((Integer)pti.primarykeyColumn.getLastPrimarykeyValue()).intValue());
		
		// Primary key type is wrong
		pti.primarykeyColumn.sqlType = Types.VARCHAR;
		try {
			gm.getNewPrimarykeyValue();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
		// Primary key is automatically generated
		pti.primarykeyColumn.isAutoGenerate = true;
		pti.primarykeyColumn.sqlType = Types.INTEGER;
		try {
			gm.getNewPrimarykeyValue();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
	}

}
