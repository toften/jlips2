/**
 * 
 */
package net.toften.jlips2.test.db;

import net.toften.jlips2.db.data.statement.Sql92StatementFactory;
import net.toften.jlips2.db.data.statement.StatementFactory;
import net.toften.jlips2.db.data.statement.StatementManager;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomas
 *
 */
public class HsqlStatementFactory extends Sql92StatementFactory implements
		StatementFactory {

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementFactory#getStatementManager(net.toften.jlips2.table.TableInfo)
	 */
	public StatementManager getStatementManager(TableInfo tableInfo) {
		
		return new HsqlStatementManager(this, tableInfo);
	}

}
