/**
 * 
 */
package net.toften.jlips2.test.db;

import net.toften.jlips2.db.data.statement.Sql92StatementManager;
import net.toften.jlips2.db.data.statement.StatementManager;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomas
 *
 */
public class HsqlStatementManager extends Sql92StatementManager implements
		StatementManager {

	/**
	 * @param sf
	 * @param tableInfo
	 */
	public HsqlStatementManager(HsqlStatementFactory sf, TableInfo tableInfo) {
		super(sf, tableInfo);
		
	}

	@Override
	public String createInsertStatement() {
		StringBuffer sb = new StringBuffer(super.createInsertStatement());
		
		if (tableInfo.primarykeyColumn.isAutoGenerate) {
			sb.append(";call identity()");
			log.debug("Insert statement ajusted for HSQL");

		}
		
		return sb.toString();
	}

}
