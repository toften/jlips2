package net.toften.jlips2.test.persist;

import java.util.Collection;

import net.toften.jlips2.Entity;

public interface Person extends Entity {
	public String getName();
	public void setName(String name);
	
	public Collection<Address> getAddress();
}
