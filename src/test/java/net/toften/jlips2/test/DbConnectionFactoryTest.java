package net.toften.jlips2.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import net.toften.jlips2.db.data.connection.ConnectionFactory;
import net.toften.jlips2.db.data.connection.ConnectionManager;
import net.toften.jlips2.db.data.connection.DbConnectionFactory;
import net.toften.jlips2.table.TableInfo;

public class DbConnectionFactoryTest extends AbstractDbWithConnectionFactoryTest {
	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionFactory.init(Properties)'
	 */
	@SuppressWarnings("unchecked")
	public void testInit() {
		DbConnectionFactory cf1 = new DbConnectionFactory();
		try {
			cf1.init(p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail();
		}
		
		// Check that the TABLEINFO has been created
		assertTrue(p.containsKey(ConnectionFactory.PROP_TABLEINFO));
		Map<String, TableInfo> t = (Map<String, TableInfo>)p.get(ConnectionFactory.PROP_TABLEINFO);
		
		checkTableInfo(t);
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionFactory.getConnectionManager()'
	 */
	public void testGetConnectionManager() {
		ConnectionManager cm = dbcf.getConnectionManager();
		
		String statement = "SELECT * FROM PERSON";
		cm.setStatement(statement);
		
		Statement s = null;
		try {
			s = cm.execute();
		} catch (SQLException e) {
			fail();
		}
		
		ResultSet rs = null;
		try {
			rs = s.getResultSet();
		} catch (SQLException e) {
			fail();
		}
		
		try {
			while (rs.next()) {
				String firstName = rs.getString("FIRSTNAME");
				assertEquals("Thomas", firstName);
				String lastName = rs.getString("LASTNAME");
				assertEquals("Larsen", lastName);
				int age = rs.getInt("AGE");
				assertEquals(37, age);
			}
		} catch (SQLException e) {
			fail();
		}
		
		try {
			cm.release();
		} catch (Exception e) {
			fail();
		}
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionFactory.getConnection()'
	 */
	public void testGetConnection() {
		Connection c = null;
		
		try {
			c = dbcf.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			fail();
		}
		
		assertEquals(1, dbcf.getNumberOfActiveConnections());

		dbcf.releaseConnection(c);
		assertEquals(0, dbcf.getNumberOfActiveConnections());
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.connection.DbConnectionFactory.releaseConnection(Connection)'
	 */
	public void testReleaseConnection() {
		try {
			dbcf.releaseConnection(null);
			fail();
		} catch (IllegalArgumentException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
	}

	private void checkTableInfo(Map<String, TableInfo> t) {
		assertTrue(t.containsKey("person"));
		
		TableInfo p = t.get("person");
		assertNotNull(p.primarykeyColumn);
		assertEquals("ID", p.primarykeyColumn.getName());
		assertEquals(4, p.columns.size());
		assertEquals(0, p.foreignkeyColumns.size());
		
		TableInfo a = t.get("address");
		assertNotNull(a.primarykeyColumn);
		assertEquals("ID", a.primarykeyColumn.getName());
		assertEquals(3, a.columns.size());
		assertEquals(1, a.foreignkeyColumns.size());
	}
}
