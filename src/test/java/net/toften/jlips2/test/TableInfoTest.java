package net.toften.jlips2.test;

import junit.framework.TestCase;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.ForeignKeyColumn;

public class TableInfoTest extends TestCase {

	/*
	 * Test method for 'net.toften.jlips2.table.DatabaseEntity.setName(String)'
	 */
	public void testSetName() {		
		TableInfo t2 = new TableInfo("tab2");
		assertNotNull(t2.getName());
		
		assertEquals("tab2", t2.getName());

		
	}
	
	public void testTableInfo() {
		TableInfo ti = new TableInfo("tab1");
		
		assertTrue(ti.columns.isEmpty());
		assertTrue(ti.foreignkeyColumns.isEmpty());
		assertNull(ti.primarykeyColumn);
		
		TableInfo.ColumnInfo ci = ti.new ColumnInfo("col1");
		assertFalse(ti.columns.isEmpty());
		assertEquals(1, ti.columns.size());
		assertTrue(ti.foreignkeyColumns.isEmpty());
		assertNull(ti.primarykeyColumn);
		assertEquals(ci, ti.columns.get(0));
		assertEquals(ti, ci.theTable);
		
		TableInfo.PrimarykeyColumn pri = ti.new PrimarykeyColumn("pri");
		assertFalse(ti.columns.isEmpty());
		assertEquals(2, ti.columns.size());
		assertTrue(ti.foreignkeyColumns.isEmpty());
		assertEquals(pri, ti.primarykeyColumn);
		assertEquals(ci, ti.columns.get(0));
		assertEquals(pri, ti.columns.get(1));
		assertEquals(ti, pri.theTable);
		
		TableInfo t2 = new TableInfo("tab2");
		ForeignKeyColumn fk = ti.new ForeignKeyColumn("fk1", t2);
		assertFalse(ti.columns.isEmpty());
		assertEquals(3, ti.columns.size());
		assertFalse(ti.foreignkeyColumns.isEmpty());
		assertEquals(ci, ti.columns.get(0));
		assertEquals(pri, ti.columns.get(1));
		assertEquals(fk, ti.columns.get(2));
		assertEquals(fk, ti.foreignkeyColumns.get(0));
		assertEquals(ti, fk.theTable);

	}

}
