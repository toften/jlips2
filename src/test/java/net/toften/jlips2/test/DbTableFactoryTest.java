package net.toften.jlips2.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.jlips2.Entity;
import net.toften.jlips2.db.DbException;
import net.toften.jlips2.db.data.connection.ConnectionFactory;
import net.toften.jlips2.db.data.connection.DbConnectionFactory;
import net.toften.jlips2.db.table.DbTableFactory;
import net.toften.jlips2.db.table.TableFactory;
import net.toften.jlips2.db.table.TableManager;
import net.toften.jlips2.entity.EntityInfo;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.persist.Dummy;
import net.toften.jlips2.test.persist.DummyNoEntity;
import net.toften.jlips2.test.stubs.PropertiesStub;

public class DbTableFactoryTest extends AbstractDbTest {	
	DbTableFactory tf;

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		PropertiesStub.addEntityMapToProperties(p);
	}
	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
		
		p.remove(EntityInfo.PROP_ENTITYMAP);
		p.remove(ConnectionFactory.PROP_TABLEINFO);
	}
	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableFactory.init(Properties)'
	 */
	public void testInit() {
		tf = new DbTableFactory();
		
		try {
			assertTrue(tf.init(p));
		} catch (Exception e) {
			fail();
		}

	}
	public void testInit2() {
		Map<Class<? extends Entity>, String> eMap1 = 
			new HashMap<Class<? extends Entity>, String>();
		eMap1.put(Dummy.class, "dummy");
		p.put(EntityInfo.PROP_ENTITYMAP, eMap1);
		
		tf = new DbTableFactory();
		
		try {
			assertTrue(tf.init(p));
			fail();
		} catch (DbException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
	
	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableFactory.getTableManager(String)'
	 */
	public void testGetTableManager() {
		tf = new DbTableFactory();
		
		try {
			assertTrue(tf.init(p));
		} catch (Exception e) {
			fail();
		}

		TableManager t1, t2;
		assertNotNull(t1 = tf.getTableManager("person"));
		assertNotNull(t2 = tf.getTableManager("PERSON"));
		assertTrue(t1 == t2);

		assertNull(tf.getTableManager("dummy"));
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableFactory.findStatementFactory()'
	 */
	public void testFindStatementFactory() {
		tf = new DbTableFactory();
		
		try {
			assertTrue(tf.init(p));
		} catch (Exception e) {
			fail();
		}

		assertNotNull(tf.findPrimarykeyFactory());

	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableFactory.findPrimarykeyFactory()'
	 */
	public void testFindPrimarykeyFactory() {
		tf = new DbTableFactory();
		
		try {
			assertTrue(tf.init(p));
		} catch (Exception e) {
			fail();
		}

		assertNotNull(tf.findStatementFactory());
		
	}

}
