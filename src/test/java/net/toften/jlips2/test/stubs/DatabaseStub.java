package net.toften.jlips2.test.stubs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class DatabaseStub {
	private String url;
	private String username;
	private String password;
	
	private static Logger sqlLog = Logger.getLogger("net.toften.jlips2.db.sql");

	
	private static String[] createStatements = {
			"CREATE TABLE person (id INTEGER IDENTITY PRIMARY KEY, firstname VARCHAR(50), lastname VARCHAR(50), age INTEGER )",
			"CREATE TABLE address (id INTEGER IDENTITY PRIMARY KEY, home VARCHAR(50), personid INTEGER, FOREIGN KEY (personid) REFERENCES person (id) )",
			"CREATE TABLE job (id INTEGER IDENTITY PRIMARY KEY, number INTEGER, name VARCHAR(50), desc VARCHAR(250) )",
			};
	
	private static String[] initialiseStatements = {
			"DELETE FROM person",
			"DELETE FROM address",
			"DELETE FROM job",
			"INSERT INTO person (firstname, lastname, age) VALUES ('Thomas', 'Larsen', 37)",
			//"INSERT INTO person (id) VALUES (null)"
			};
	
	private static void executeStatementArray(DatabaseStub dbs, String[] statements) throws SQLException {
		Connection c = dbs.getConnection();
		
		for (String expression : statements) {
			Statement s = c.createStatement();
			
			int result = s.executeUpdate(expression);
			if (result == -1) {
				throw new SQLException("db error : " + expression);
			}
			
			sqlLog.info(expression);
			
			s.close();
		}
		
		c.close();
		
	}
	
	public static DatabaseStub CreateDatabase(String url, String username, String password) throws ClassNotFoundException, SQLException {
        Class.forName("org.hsqldb.jdbcDriver");
        
        DatabaseStub dbs = new DatabaseStub();
        dbs.url = url;
        dbs.username = username;
        dbs.password = password;
        
        executeStatementArray(dbs, createStatements);
        
        return dbs;
	}
	
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
	
	public void initialiseDatabase() throws SQLException {
        executeStatementArray(this, initialiseStatements);
	}
	
	public int getLastUsedPrimarykeyValue(String tableName) throws SQLException {
    	int lastPk = 0;
		String sql = "select max(id) from " + tableName;
    	
    	ResultSet rs = getConnection().createStatement().executeQuery(sql);
    	while (rs.next()) {
    		lastPk = rs.getInt(1);
    	}

    	return lastPk;
	}
}
