package net.toften.jlips2.test.stubs;

import java.sql.Types;

import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.ColumnInfo;
import net.toften.jlips2.table.TableInfo.ForeignKeyColumn;
import net.toften.jlips2.table.TableInfo.PrimarykeyColumn;

public class TableInfoStub {
	/**
	 * This stub will return a TableInfo object for a table
	 * representing a person.
	 * 
	 * The SQL definition for the table is:
	 * 
	 * =========================================
	 * = Table: person                         =
	 * =========================================
	 * id           INTEGER         PK (auto generate)
	 * firstname    VARCHAR(50)
	 * lastname     VARCHAR(50)
	 * age          INTEGER
	 * 
	 * @return TableInfo object representing the table
	 */
	public static TableInfo getPersonTableInfo() {
		TableInfo p = new TableInfo("person");
		
		PrimarykeyColumn pk = p.new PrimarykeyColumn("id");
		pk.sqlType = Types.INTEGER;
		pk.isAutoGenerate = true;
		
		ColumnInfo c1 = p.new ColumnInfo("firstname");
		c1.columnSize = 50;
		c1.sqlType = Types.VARCHAR;
		
		ColumnInfo c2 = p.new ColumnInfo("lastname");
		c2.columnSize = 50;
		c2.sqlType = Types.VARCHAR;

		ColumnInfo c3 = p.new ColumnInfo("age");
		c3.sqlType = Types.INTEGER;
		
		return p;
	}

	/**
	 * This stub will return a TableInfo object for a table
	 * representing a job.
	 * 
	 * The SQL definition for the table is:
	 * 
	 * =========================================
	 * = Table: job                            =
	 * =========================================
	 * id           INTEGER         PK (auto generate)
	 * number       INTEGER
	 * name         VARCHAR(50)
	 * desc         VARCHAR(250)
	 * 
	 * @return TableInfo object representing the table
	 */
	public static TableInfo getJobTableInfo() {
		TableInfo j = new TableInfo("job");
		
		PrimarykeyColumn pk = j.new PrimarykeyColumn("id");
		pk.sqlType = Types.INTEGER;
		pk.isAutoGenerate = true;
		
		ColumnInfo c1 = j.new ColumnInfo("number");
		c1.sqlType = Types.INTEGER;
		
		ColumnInfo c2 = j.new ColumnInfo("name");
		c2.columnSize = 50;
		c2.sqlType = Types.VARCHAR;
		
		ColumnInfo c3 = j.new ColumnInfo("desc");
		c3.columnSize = 250;
		c3.sqlType = Types.VARCHAR;

		return j;
	}
	
	/**
	 * This stub will return a TableInfo object for a table
	 * representing a address.
	 * 
	 * The SQL definition for the table is:
	 * 
	 * =========================================
	 * = Table: address                        =
	 * =========================================
	 * id 			INTEGER IDENTITY PRIMARY KEY, 
	 * home 		VARCHAR(50)
	 * personid 	INTEGER, FOREIGN KEY (personid) REFERENCES person (id)
	 * 
	 * @return TableInfo object representing the table
	 */
	public static TableInfo getAddressTableInfo(TableInfo pti) {
		TableInfo a = new TableInfo("address");
		
		PrimarykeyColumn pk = a.new PrimarykeyColumn("id");
		pk.sqlType = Types.INTEGER;
		pk.isAutoGenerate = true;
		
		ColumnInfo c1 = a.new ColumnInfo("home");
		c1.columnSize = 50;
		c1.sqlType = Types.VARCHAR;

		ForeignKeyColumn f1 = a.new ForeignKeyColumn("personid", pti);
		f1.sqlType = Types.INTEGER;
		
		return a;
	}


}
