package net.toften.jlips2.test.stubs;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.jlips2.Entity;
import net.toften.jlips2.entity.EntityInfo;
import net.toften.jlips2.test.persist.Person;

public class PropertiesStub {
	static Map<Class<? extends Entity>, String> em = new HashMap<Class<? extends Entity>, String>();
	
	static {
		em.put(Person.class, "person");
	}

	public static Properties getPropertiesWithEntityMap() {
		Properties p = new Properties();
		
		p.put(EntityInfo.PROP_ENTITYMAP, em);
		
		return p;
	}
	
	public static void addEntityMapToProperties(Properties p) {
		p.put(EntityInfo.PROP_ENTITYMAP, em);
		
	}

}
