package net.toften.jlips2.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.db.data.statement.StatementFactory;
import net.toften.jlips2.record.DefaultRecordFactory;
import net.toften.jlips2.record.DefaultRecordManager;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.record.RecordManager;
import net.toften.jlips2.test.stubs.PropertiesStub;

public class DefaultRecordManagerTest extends AbstractDbTest {
	DefaultRecordFactory 	rf;
	DefaultRecordManager 	rm;
	
	ID id;
	
	protected void setUp() throws Exception {
		super.setUp();
		
		PropertiesStub.addEntityMapToProperties(p);
		p.put(StatementFactory.PROP_STATEMENTFACTORY, "net.toften.jlips2.test.db.HsqlStatementFactory");

		// Create a factory to use
		rf = new DefaultRecordFactory();
		try {
			assertTrue(rf.init(p));
		} catch (Exception e) {
			fail();
		}
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.DbRecordManager(DefaultRecordFactory, String)'
	 */
	public void testDbRecordManager() {
		RecordManager localRm = rf.getRecordManger("person");
		
		assertTrue(localRm instanceof DefaultRecordManager);
		rm = (DefaultRecordManager)localRm;
		
		assertNotNull(rm);
	}
	
	public void testDbRecordManager2() {
		try {
			rf.getRecordManger("dummy");
			fail();
		}
		catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}
		
	}


	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.create()'
	 */
	public void testCreate() {
		testDbRecordManager(); // Create a record manager

		int lastPk = getLastPrimarykeyValueFromDatabase("person");
		
		id = rm.create();
		assertNotNull(id);
		assertTrue(id instanceof PrimaryKeyManager);
		assertEquals(lastPk + 1, ((PrimaryKeyManager)id).getValue());
		assertEquals(lastPk + 1, getLastPrimarykeyValueFromDatabase("person"));
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.find(ID)'
	 */
	public void testFind() {
		testDbRecordManager(); // Create a record manager

		int lastPk = getLastPrimarykeyValueFromDatabase("person");
		id = Ident.newIntegerId(lastPk);
		
		ID newId = rm.find(id);
		assertNotNull(newId);
		assertTrue(newId instanceof PrimaryKeyManager);
		assertEquals(lastPk, ((PrimaryKeyManager)newId).getValue());
		
		id = newId; // Pass it on for other tests to use

	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.remove()'
	 */
	public void testRemove() {
		int startRecordCount = recordCount;
		
		testFind();
		rm.remove();
		assertEquals(startRecordCount - 1, getNumberOfRecords("person"));
		
		try {
			rm.remove();
			fail();
		} catch (IllegalStateException e) {
			// expected exception
		} catch (Exception e) {
			fail();
		}

	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.commit()'
	 */
	public void testCommit() {
		testSet();
		
		rm.commit();
		
		// Test that it NOW has been updated in the database	
		int foundPrimarykey = (Integer)((PrimaryKeyManager)id).getValue();
		String sql = "select * from person where id=" + foundPrimarykey;
		
		try {
			Connection c = dbs.getConnection();
			ResultSet rs = c.createStatement().executeQuery(sql);
			while (rs.next()) {
				assertEquals(rm.get("lastname"), rs.getString("lastname"));
				assertEquals("Andersen", rm.get("lastname"));
				assertEquals("Andersen", rs.getString("lastname"));
			}
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.rollback()'
	 */
	public void testRollback() {
		testSet();
		
		rm.rollback();
		
		assertEquals("Larsen", rm.get("lastname"));
		
		// Test that it is the same as the RecordManager
		int foundPrimarykey = (Integer)((PrimaryKeyManager)id).getValue();
		String sql = "select * from person where id=" + foundPrimarykey;
		
		try {
			Connection c = dbs.getConnection();
			ResultSet rs = c.createStatement().executeQuery(sql);
			while (rs.next()) {
				assertEquals(rm.get("lastname"), rs.getString("lastname"));
				assertEquals("Larsen", rm.get("lastname"));
				assertEquals("Larsen", rs.getString("lastname"));
			}
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.set(String, Object)'
	 */
	public void testSet() {
		testGet();
		
		rm.set("lastname", "Andersen");
		assertEquals("Andersen", rm.get("lastname"));
		
		// Test that it hasn't been updated in the database	
		int foundPrimarykey = (Integer)((PrimaryKeyManager)id).getValue();
		String sql = "select * from person where id=" + foundPrimarykey;
		
		try {
			Connection c = dbs.getConnection();
			ResultSet rs = c.createStatement().executeQuery(sql);
			while (rs.next()) {
				assertNotSame(rm.get("lastname"), rs.getString("lastname"));
				assertEquals("Larsen", rs.getString("lastname"));
			}
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.get(String)'
	 */
	public void testGet() {
		testFind();
		
		int foundPrimarykey = (Integer)((PrimaryKeyManager)id).getValue();
		String sql = "select * from person where id=" + foundPrimarykey;
		
		try {
			Connection c = dbs.getConnection();
			ResultSet rs = c.createStatement().executeQuery(sql);
			while (rs.next()) {
				assertEquals(rs.getString("lastname"), rm.get("lastname"));
				assertEquals(rs.getString("firstname"), rm.get("firstname"));
				assertEquals(rs.getInt("age"), rm.get("age"));
			}
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordManager.isDirty()'
	 */
	public void testIsDirty() {
		testGet();
		assertFalse(rm.isDirty());
		
		testSet();
		assertTrue(rm.isDirty());
		
		rm.rollback();
		assertFalse(rm.isDirty());
		
		rm.set("lastname", "Andersen");
		assertTrue(rm.isDirty());
		rm.commit();
		assertFalse(rm.isDirty());
	}

}
