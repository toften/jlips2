package net.toften.jlips2.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import net.toften.jlips2.db.DbException;
import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.db.data.statement.StatementFactory;
import net.toften.jlips2.db.table.DbTableFactory;
import net.toften.jlips2.db.table.DbTableManager;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.PropertiesStub;
import net.toften.jlips2.test.stubs.TableInfoStub;


public class DbTableManagerTest extends AbstractDbTest {
	DbTableFactory 	tf;
	DbTableManager 	tm;
	
	ID 				id 			= null;
	TableInfo 		ti 			= null;
	int 			lastPK;

	protected void setUp() throws Exception {
		super.setUp();
		
		PropertiesStub.addEntityMapToProperties(p);
		p.put(StatementFactory.PROP_STATEMENTFACTORY, "net.toften.jlips2.test.db.HsqlStatementFactory");

		// Create a factory to use
		tf = new DbTableFactory();
		try {
			assertTrue(tf.init(p));
		} catch (Exception e) {
			fail();
		}

		// Setup helper attributes
		tm 		= (DbTableManager)tf.getTableManager("person");
		ti 		= tm.getTableInfo();
		lastPK 	= getLastPrimarykeyValue(ti);
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableManager.DbTableManager(DbTableFactory, TableInfo)'
	 */
	public void testDbTableManager() {
		TableInfo pti = TableInfoStub.getPersonTableInfo();
		tm = new DbTableManager(tf, pti);
		assertNotNull(tm);
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableManager.insertRecord()'
	 */
	public void testInsertRecord() {
		try {
			id = tm.insertRecord();
		} catch (DbException e) {
			fail();
		}
		assertNotNull(id);
		assertTrue(id instanceof PrimaryKeyManager);
		assertEquals(lastPK + 1, ((PrimaryKeyManager)id).getValue());
		assertEquals(recordCount + 1, getNumberOfRecords("person"));

	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableManager.updateRecord(ID, Map<String, Object>)'
	 */
	public void testUpdateRecord() {
		testInsertRecord();
		
		int newPk = (Integer)((PrimaryKeyManager)id).getValue();
		assertEquals(lastPK + 1, newPk);
		
		// Try and update
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("lastname", "Andersen");
		values.put("fIrStNaMe", "Hans Christian");
		values.put("dummy", "dummy");
		
		try {
			tm.updateRecord(id, values);
		} catch (DbException e) {
			fail();
		}
		
		// validate
		try {
			Connection c = dbs.getConnection();
			ResultSet rs = c.createStatement().executeQuery("select * from person where id = " + newPk);
			
			while (rs.next()) {
				String lastname = rs.getString("lastname");
				assertEquals("Andersen", lastname);
				String firstname = rs.getString("firstname");
				assertEquals("Hans Christian", firstname);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void testUpdateRecord2() throws Exception {
		PrimaryKeyManager badId = tf.findPrimarykeyFactory().getPrimarykeyManager(ti);
		
		// Try and update
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("dummy", "dummy");
		
		try {
			tm.updateRecord(badId, values);
			fail();
		} catch (IllegalStateException e) {
			// Expected exception
		} catch (Exception e) {
			fail();
		}
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableManager.deleteRecord(ID)'
	 */
	public void testDeleteRecord() {
		testInsertRecord();
		
		try {
			tm.deleteRecord(id);
		} catch (Exception e) {
			fail();
		}
	}
	
	public void testDeleteRecord2() throws Exception {
		PrimaryKeyManager badId = tf.findPrimarykeyFactory().getPrimarykeyManager(ti);
		
		try {
			tm.deleteRecord(badId);
			fail();
		} catch (IllegalStateException e) {
			// Expected exception
		} catch (Exception e) {
			fail();
		}
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.table.DbTableManager.getRecord(ID, Map<String, Object>)'
	 */
	public void testGetRecord() {
		id = Ident.newIntegerId(lastPK);

		Map<String, Object> values = new HashMap<String, Object>();
		
		try {
			tm.getRecord(id, values);
		} catch (Exception e) {
			fail();
		}
		Object lastname = values.get("lastname");
		Object firstnameBad = values.get("fIrStNaMe");
		Object firstname = values.get("firstname");
		Object age = values.get("age");
		Object dummy = values.get("dummy");
		
		assertNotNull(lastname);
		assertNotNull(firstname);
		assertNotNull(age);
		assertNull(firstnameBad);
		assertNull(dummy);
		
		assertEquals("Thomas", firstname);
		assertEquals("Larsen", lastname);
		assertEquals(37, age);

	}

	public void testgetRecord2() throws Exception {
		PrimaryKeyManager badId = tf.findPrimarykeyFactory().getPrimarykeyManager(ti);
		
		Map<String, Object> values = new HashMap<String, Object>();
		
		try {
			tm.getRecord(badId, values);
			fail();
		} catch (IllegalStateException e) {
			// Expected exception
		} catch (Exception e) {
			fail();
		}
		
	}

}
