package net.toften.jlips2.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.toften.jlips2.db.data.connection.DbConnectionFactory;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.DatabaseStub;

public class AbstractDbTest extends AbstractFactoryTest {
	protected static String databaseName = "jlips";
	protected static String url = "jdbc:hsqldb:mem:" + databaseName;
	protected static String username = "sa";
	protected static String password = "";
	
	protected static DatabaseStub dbs = null;
	
	static {
		p.put(DbConnectionFactory.PROP_URL, url);
		p.put(DbConnectionFactory.PROP_USERNAME, username);
		p.put(DbConnectionFactory.PROP_PASSWORD, password);
		
		try {
			dbs = DatabaseStub.CreateDatabase(url, username, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected int recordCount;
	
	protected static int getNumberOfRecords(String tableName) {
		int returnValue = 0;
		
		try {
			Connection c = dbs.getConnection();
			ResultSet rs = c.createStatement().executeQuery("select count(*) from " + tableName);
			
			while (rs.next()) {
				returnValue = rs.getInt(1);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnValue;
		
	}

	/**
	 * @param tableInfo TODO
	 * 
	 */
	protected static int getLastPrimarykeyValue(TableInfo tableInfo) {
		return (Integer)tableInfo.primarykeyColumn.getLastPrimarykeyValue();
	}

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		
		dbs.initialiseDatabase();
				
		recordCount 	= getNumberOfRecords("person");
	}

	/**
	 * @param tableName TODO
	 * @return
	 */
	protected static int getLastPrimarykeyValueFromDatabase(String tableName) {
		int lastPk = 0;
		
		try {
			lastPk = dbs.getLastUsedPrimarykeyValue(tableName);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return lastPk;
	}
	
}
