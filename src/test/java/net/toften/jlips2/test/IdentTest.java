package net.toften.jlips2.test;

import junit.framework.TestCase;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.record.IntegerIdent;
import net.toften.jlips2.record.StringIdent;

public class IdentTest extends TestCase {

	/*
	 * Test method for 'net.toften.jlips2.record.Ident.newIntegerId(int)'
	 */
	public void testNewIntegerId() {
		ID i = Ident.newIntegerId(23);
		assertTrue(i instanceof ID);
		assertTrue(i instanceof Ident);
		assertTrue(i instanceof IntegerIdent);
		assertFalse(i instanceof StringIdent);
		
		assertEquals(23, ((IntegerIdent)i).getId());
	}

	/*
	 * Test method for 'net.toften.jlips2.record.Ident.newStringId(String)'
	 */
	public void testNewStringId() {
		ID i = Ident.newStringId("unique");
		assertTrue(i instanceof ID);
		assertTrue(i instanceof Ident);
		assertTrue(i instanceof StringIdent);
		assertFalse(i instanceof IntegerIdent);
		
		assertEquals("unique", ((StringIdent)i).getId());
	}

}
