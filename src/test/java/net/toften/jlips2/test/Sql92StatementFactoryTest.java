package net.toften.jlips2.test;

import net.toften.jlips2.db.data.statement.Sql92StatementFactory;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.test.stubs.PropertiesStub;
import net.toften.jlips2.test.stubs.TableInfoStub;

public class Sql92StatementFactoryTest extends AbstractDbTest {
	static {
		PropertiesStub.addEntityMapToProperties(p);
	}
	
	Sql92StatementFactory s92sf;	

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		s92sf = new Sql92StatementFactory();
		s92sf.init(p);

	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementFactory.init(Properties)'
	 */
	public void testInit() {
		Sql92StatementFactory sf = new Sql92StatementFactory();
		try {
			assertTrue(sf.init(p));
		} catch (Exception e) {
			fail();
		}
		
	}

	/*
	 * Test method for 'net.toften.jlips2.db.data.statement.Sql92StatementFactory.getStatementManager(String)'
	 */
	public void testGetStatementManager() {
		TableInfo pti = TableInfoStub.getPersonTableInfo();
		
		assertNotNull(s92sf.getStatementManager(pti));
	}

}
