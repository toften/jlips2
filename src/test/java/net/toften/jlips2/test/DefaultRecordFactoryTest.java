package net.toften.jlips2.test;

import net.toften.jlips2.record.DefaultRecordFactory;
import net.toften.jlips2.test.stubs.PropertiesStub;

public class DefaultRecordFactoryTest extends AbstractDbTest {
	static {
		PropertiesStub.addEntityMapToProperties(p);
	}
	
	DefaultRecordFactory rf;

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordFactory.init(Properties)'
	 */
	public void testInit() {
		rf = new DefaultRecordFactory();
		
		try {
			assertTrue(rf.init(p));
		} catch (Exception e) {
			fail();
		}

	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordFactory.getRecordManger(String)'
	 */
	public void testGetRecordManger() {
		testInit();
		
		assertNotNull(rf.getRecordManger("person"));
	}

	/*
	 * Test method for 'net.toften.jlips2.db.record.DbRecordFactory.findTableFactory(String)'
	 */
	public void testFindTableFactory() {
		testInit();
		
		assertNotNull(rf.findTableFactory());
		
	}

}
