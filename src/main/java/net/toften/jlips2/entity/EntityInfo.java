package net.toften.jlips2.entity;
import net.toften.jlips2.Entity;
import net.toften.jlips2.record.RecordFactory;

/**
 * This class contains general information about an entity.  The main information
 * is the mapping between the entity interface and a tablename. Information also
 * contained is the RecordFactory that must be used to create RecordManagers for
 * the entity. The EntityInfo is used by the EntityManager to create an
 * association from the EntityManager to a RecordManager.
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:21
 */
public class EntityInfo {
	public static final String PROP_ENTITYMAP = "entityMap";
	/**
	 * The name of the table the entiry is associated with
	 */
	private String tableName;
	/**
	 * The EntityClass
	 */
	private Class<? extends Entity> entityClass;
	/**
	 * The Record manager for the entity
	 */
	private RecordFactory rf;

	/**
	 * Constructor for the EntityInfo object
	 * 
	 * @param entityClass    The entity interface class
	 * @param tableName    The name of the table that the entity is associated with
	 * @param rf    The RecordFactory that should be used to create
	 * RecordManagers with by the EntityManager
	 */
	public EntityInfo(Class<? extends Entity> entityClass, String tableName, RecordFactory rf){
		this.entityClass = entityClass;
		this.tableName = tableName;
		this.rf = rf;
	}

	/**
	 * Returns the class of the entity interface
	 */
	public Class<? extends Entity> getEntityClass(){
		return entityClass;
	}

	/**
	 * Returns the tablename associated with the entity interface
	 */
	public String getTableName(){
		return tableName;
	}

	/**
	 * Returns the RecordFactory used to create RecordManagers
	 */
	public RecordFactory getTheManagerFactory(){
		return rf;
	}

	/**
	 * This method will return true if the entity passed
	 * in are persisted in the same database table.
	 * 
	 * The criteria for being persisted in the same table are:
	 * <ul>
	 * <li>Having the same table name</li>
	 * <li>Using the same RecordFactory</li>
	 * </ul>
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (!(obj instanceof EntityInfo))
			return false;
		
		EntityInfo e = (EntityInfo)obj;
		
		if (!getTableName().equals(e.getTableName()))
			return false;
		
		if (getTheManagerFactory() != e.getTheManagerFactory())
			return false;
		
		return true;
	}

}