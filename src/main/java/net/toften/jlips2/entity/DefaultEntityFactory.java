package net.toften.jlips2.entity;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.jlips2.Entity;
import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.record.RecordFactory;

/**
 * @created 25-Sep-2005 16:34:21
 * @author thomaslarsen
 * @version 1.0
 */
public class DefaultEntityFactory extends LayerFactory implements EntityFactory {

	/**
	 * The record managers are a Map that maps the entity interfaces with an
	 * EntityInfo Object.  Each entity interface can have different RecordManagers.
	 * When a EntityHandler is created for a specific entity interface, the associated
	 * EntityInfo object contains a reference to a RecordFactory. This factory is used
	 * to create the appropriate RecordManager.
	 */
	private static Map<Class<? extends Entity>, EntityInfo> entityMap = new HashMap<Class<? extends Entity>, EntityInfo>();
	/**
	 * This is the default RecordFactory to use if no other RecordFactory is specified
	 * in the init properties.
	 */
	public static final String DEFAULT_RECORDS_FACTORY = "net.toften.jlips2.record.DefaultRecordFactory";
	public static final String DEFAULT_COLLECTIONS_FACTORY = "net.toften.jlips2.record.DefaultCollectionFactory";

	/**
	 * Initialises the EntityFactory.  The initialisation will populate the {@link
	 * #entityMap} Map. The EntityInfo objects in the map will be created using
	 * information taken from the init properties.
	 * 
	 * @param prop    The init properties
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public boolean init(Properties prop) throws Exception {
		RecordFactory rf = LayerFactory.initialiseFactory(RecordFactory.PROP_RECORDFACTORY,DEFAULT_RECORDS_FACTORY, prop, RecordFactory.class);

		boolean returnValue = false;

		/*
		 * Create the entityMap
		 */
		Map<Class<? extends Entity>, String> em = (Map<Class<? extends Entity>, String>) prop.get(EntityInfo.PROP_ENTITYMAP);
		
		if (em != null) {
			Class<? extends Entity>[] keys = (Class<? extends Entity>[]) em.keySet().toArray();
			
			for (int i = 0; i < keys.length; i++) {
				Class<? extends Entity> entityClass = keys[i];
				String tableName = em.get(entityClass);
				
				EntityInfo ei = new EntityInfo(entityClass, tableName, rf);
				
				entityMap.put(entityClass, ei);
			}
			
			returnValue = true;
		}
		
		return returnValue;
	}

	/**
	 * Returns a new EntityManager for a specified entity interface
	 * 
	 * @param entityClass    The entity interface class to return a EntityManager for
	 */
	public EntityManager getEntityManager(Class<? extends Entity> entityClass){
		
		return new DefaultEntityManager(entityClass);
	}

	/**
	 * Returns a EntityInfo object for a specified entity interface
	 * 
	 * @param className    The entity interface class to return a EntityInfo for
	 */
	public static EntityInfo findEntityInfo(Class<? extends Entity> entityClass){
		return entityMap.get(entityClass);
	}

}