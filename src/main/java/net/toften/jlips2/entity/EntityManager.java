package net.toften.jlips2.entity;

import net.toften.jlips2.Entity;
import net.toften.jlips2.record.ID;

/**
 * The EntityManager represents one entity.  The EntityManager is responsible for
 * implementing the methods defined in the entity interface aswell as the
 * lifecycle methods responsible for instantiating the entity:
 * <ul>
 * <li>Creation</li>
 * <li>Locating (find)</li>
 * </ul>  The implementation of the EntityManager must be associated with a
 * mechanism that enables the actual changes to the persisted entity. This is
 * normally done by associating it with an implementation of the {@link
 * RecordHandler} interface.
 * @created 25-Sep-2005 16:59:17
 * @author thomaslarsen
 * @version 1.0
 */
public interface EntityManager {
	public static final String PROP_ENTITYMANAGER = "net.toften.jlips2.entity.EntityManager";

	/**
	 * 
	 * @param T
	 */
	public <T extends Entity> T create();

	/**
	 * 
	 * @param T    T
	 */
	public <T extends Entity> T find(ID id);

}