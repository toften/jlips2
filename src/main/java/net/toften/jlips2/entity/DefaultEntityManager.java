package net.toften.jlips2.entity;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.toften.jlips2.Entity;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.RecordManager;

/**
 * The default implementation of the EntitiManager.  This implementation will
 * provide functionality to intercept the method calls to the entity interface. It
 * will also provide an implementation of the lifecycle methods defined in the
 * {@link EntityManager} interface.
 * 
 * @created 25-Sep-2005 16:59:17
 * @author thomaslarsen
 * @version 1.0
 */
public class DefaultEntityManager implements EntityManager, InvocationHandler {

	private Object proxy = null;

	/**
	 * List that holds the EntityInfo object for the persist
	 * entities in the inheritance hierarcht of the entity
	 * interface.
	 * 
	 * The first element in the list is the entity interface
	 * itself, then followed by it's super-interface and so forth.
	 * The last element in the list is the entity interface just
	 * "above" the {@link Entity} interface
	 */
	private List<EntityInfo> entityInfoList = 
		new LinkedList<EntityInfo>();
	
	/**
	 * This Map is used to map between the entity interfaces and
	 * the record managers
	 */
	private Map<Class<? extends Entity>, RecordManager> recordManagers = 
		new LinkedHashMap<Class<? extends Entity>, RecordManager>();
	
	/**
	 * This Map maps between the entity interfaces and the EntityInfo
	 * objects
	 */
	private Map<Class<? extends Entity>, EntityInfo> entityInfoMap =
		new LinkedHashMap<Class<? extends Entity>, EntityInfo>();
	
	/**
	 * @param s
	 * @return
	 */
	private static String[] split(String s) {
		Set<String> parts = new LinkedHashSet<String>();
		
        int lastUC = 0;
        
        for (int i = 0; i < s.length(); i++) {
            if (Character.isUpperCase(s.charAt(i))) {
                parts.add(s.substring(lastUC, i).toLowerCase());
                
                lastUC = i;
            }
        }
        if (lastUC < s.length())
            parts.add(s.substring(lastUC, s.length()).toLowerCase());
				
		String[] a = new String[parts.size()];
		parts.toArray(a);
		
		return a;
	}
	
	/**
	 * 
	 * @param entityClass
	 */
	@SuppressWarnings("unchecked")
	public DefaultEntityManager(Class<? extends Entity> entityClass){
		// Loop through inheritance hierarchy
		Class<? extends Entity> top = entityClass;
		
		while (top != Entity.class) {
			entityInfoList.add(DefaultEntityFactory.findEntityInfo(top));
			
			top = (Class<? extends Entity>) top.getSuperclass();
		}
		
		Collections.reverse(entityInfoList); // Reverse to get the entity interface to the top
		for (EntityInfo ei : entityInfoList) {
			entityInfoMap.put(ei.getEntityClass(), ei);		
		}
		
		/*
		 * Create the proxy
		 */
		proxy = Proxy.newProxyInstance(entityClass.getClassLoader(), new Class<?>[] { entityClass }, this);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.entity.EntityManager#create()
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> T create() {
		RecordManager lastRm = null;
		EntityInfo lastEi = null;
		
		for (EntityInfo ei : entityInfoList) {
			RecordManager newRm = null;
			if (lastEi == null) {
				newRm = ei.getTheManagerFactory().getRecordManger(ei.getTableName());
			} else if (!ei.equals(lastEi)) {
				newRm = ei.getTheManagerFactory().getRecordManger(ei.getTableName());
			}
				
			if (newRm != lastRm) {
				ID id = newRm.create();
				
				// Set the pointer to the new object
				if (lastRm != null) {
					lastRm.set(fieldName, id);
				}
				
				lastRm = newRm;
			}
			recordManagers.put(ei.getEntityClass(), lastRm);
			
			lastEi = ei;
			
		}
		
		return (T)proxy;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.entity.EntityManager#find(net.toften.jlips2.record.ID)
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> T find(ID id) {
		RecordManager lastRm = null;
		EntityInfo lastEi = null;
		ID nextId = id;
		
		for (EntityInfo ei : entityInfoList) {
			RecordManager newRm = null;
			
			if (lastEi == null) {
				newRm = ei.getTheManagerFactory().getRecordManger(ei.getTableName());
			} else if (!ei.equals(lastEi)) {
				newRm = ei.getTheManagerFactory().getRecordManger(ei.getTableName());
				nextId = lastRm.getField(fieldName);
			}
				
			if (newRm != lastRm) {
				newRm.find(nextId);
				
				lastRm = newRm;
			}
			recordManagers.put(ei.getEntityClass(), lastRm);
			
			lastEi = ei;
			
		}
		
		return (T)proxy;
	}

	/**
	 * This method catches all calls to the proxy. The responsiblity of the proxy
	 * is to implement all methods defined in the entity interface
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @exception Throwable
	 */
	public Object invoke(Object arg0, Method arg1, Object[] arg2) throws Throwable {
		Object returnValue = null;
		
		String methodName = arg1.getName();
		Class returnType = arg1.getReturnType();
		
		Class c = arg1.getDeclaringClass();
		EntityInfo ei = entityInfoMap.get(c);
		RecordManager rm = recordManagers.get(c);
		
		if (c == Entity.class) {
			// TODO Pass on the method directly to the RecordHandler
		} else {
			String[] methodParts = split(methodName);
			
			String command = methodParts[0];
			String fieldName = methodParts[1];
			String role = methodParts[1];
			
			if (command.equals("set")) {
				Object value = arg2[0];
				
				rm.set(fieldName, value);
			}
			
			if (command.equals("get")) {
				returnValue = rm.get(fieldName);
			}
		}
		
		return returnValue;
	}

}