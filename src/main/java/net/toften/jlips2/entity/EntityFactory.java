package net.toften.jlips2.entity;
import net.toften.jlips2.BaseFactory;
import net.toften.jlips2.Entity;

/**
 * Ost
 * @created 25-Sep-2005 16:59:17
 * @author thomaslarsen
 * @version 1.0
 */
public interface EntityFactory extends BaseFactory {
	public static final String PROP_ENTITYFACTORY = "net.toften.jlips2.entity.EntityFactory";

	/**
	 * 
	 * @param entityClass    entityClass
	 */
	public EntityManager getEntityManager(Class<? extends Entity> entityClass);

}