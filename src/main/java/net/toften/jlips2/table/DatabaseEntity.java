package net.toften.jlips2.table;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 05-Oct-2005 10:27:44
 */
public abstract class DatabaseEntity {

	private String name;
	
	public DatabaseEntity(String name) {
		this.name = name;
	}

	public String getName(){
		return name;
	}

}