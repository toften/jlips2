package net.toften.jlips2;

import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * This is the base class for all factories.
 * 
 * This class provides support for all the factories, as well as
 * provide a number of logging facilities.
 * 
 * @created 25-Sep-2005 16:34:21
 * @author thomaslarsen
 * @version 1.0
 */
public abstract class LayerFactory implements BaseFactory {
	
	static {
		/*
		 * Initialisation of log4j
		 */
		BasicConfigurator.configure();
	}
	
	/**
	 * This logger should be used by specialising factories to 
	 * log messages that is related to any database activity
	 */
	protected static Logger dbLogger = Logger.getLogger("net.toften.jlips.db");
	
	/**
	 * This is the default logger in jLips2. All non-specialised logging
	 * should be made to this logger
	 * 
	 * @see #dbLogger
	 */
	protected static Logger defLogger = Logger.getLogger("net.toften.jlips");

	/**
	 * The default EntityFactory factory
	 */
	public static final String DEFAULT_ENTITYFACTORY = "net.toften.jlips2.entity.DefaultEntityFactory";

	/* (non-Javadoc)
	 * @see net.toften.jlips2.BaseFactory#init(java.util.Properties)
	 */
	public abstract boolean init(Properties prop) throws Exception;

	/**
	 * This method will initialise a factory and invoke it's init method
	 * 
	 * The initialisation method will return a factory interface type
	 * which the factory must implement
	 * 
	 * @param <T> The factory interface that must be returned
	 * @param factoryClassName The name of the factory class
	 * @param prop The properties passed into jLips
	 * @param factoryInterface The factory interface class
	 * @return The initialised factory instance
	 * @throws Exception Any exception can be thrown
	 */
	@SuppressWarnings("unchecked")
	public static <T extends BaseFactory> T initialiseFactory(String factoryClassName, Properties prop, Class<T> factoryInterface) throws Exception {
		T newFactory = null;
		Class factoryClass = null;
		
		// Try and load the provided factory class
		factoryClass = Class.forName(factoryClassName);
		
		/*
		 * The new factory is instantiated
		 * Note that the instance must be cast to the correct
		 * interface type
		 */
		newFactory = (T)factoryClass.newInstance();
		
		// Call the factory's init method
		newFactory.init(prop);
		
		defLogger.info(factoryClass.getName() + " factory initialised");
		
		return newFactory;
	}
	
	/**
	 * This initialise method is identical to the other initialise method
	 * except that a the factory class name is read from the provided
	 * properties
	 * 
	 * If the properties does not contain a value of the provided key and no
	 * default factory class name is provided, the method returns null
	 * 
	 * @param <T> The factory interface that must be returned
	 * @param factoryPropKey The property key that holds the factory class name
	 * @param defaultFactoryClassName The factory class name to use if the properties
	 * 								does not contain the specified key
	 * @param prop The properties provided to jLips2
	 * @param factoryInterface The factory interface class
	 * @return The initialised factory instance or null of class not found
	 * @throws Exception Any exception can be thrown
	 */
	public static <T extends BaseFactory> T initialiseFactory(String factoryPropKey, String defaultFactoryClassName, Properties prop, Class<T> factoryInterface) throws Exception{
		T returnValue = null;
		
		String entityFactoryClassName = defaultFactoryClassName;
		
		if (prop != null)
			entityFactoryClassName = prop.getProperty(factoryPropKey, defaultFactoryClassName);
		
		if (entityFactoryClassName != null) { // Only if we have found a class name
			returnValue = initialiseFactory(entityFactoryClassName, prop, factoryInterface);
		} else {
			defLogger.warn("Factory class name not found when initialising a " + factoryInterface.getName());
		}
		
		return returnValue;
	}

}