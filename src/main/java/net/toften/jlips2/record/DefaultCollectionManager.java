package net.toften.jlips2.record;

import java.util.Collection;

import net.toften.jlips2.db.table.TableFactory;

public class DefaultCollectionManager implements CollectionManager {
	private DefaultRecordFactory rf;
	private String tableName;
	private TableFactory tf;
	
	public DefaultCollectionManager(DefaultRecordFactory rf, String tableName) {
		this.rf = rf;
		this.tableName = tableName;
		
		tf = rf.findTableFactory();
		
		// Test to see if a TableManager exists		
		if (tf.getTableManager(tableName) == null)
			throw new IllegalStateException("Can not find TableManager for table: " + tableName);

	}

	public Collection<ID> find(String relatedColumn, Object value) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
