package net.toften.jlips2.record;
import java.util.Properties;

import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.db.table.TableFactory;

/**
 * @created 26-Sep-2005 21:57:38
 * @author thomaslarsen
 * @version 1.0
 */
public class DefaultRecordFactory extends LayerFactory implements RecordFactory {

	public static final String DEFAULT_TABLE_FACTORY = "net.toften.jlips2.db.table.DbTableFactory";

	private TableFactory tf;

	/* (non-Javadoc)
	 * @see net.toften.jlips2.BaseFactory#init(java.util.Properties)
	 */
	public boolean init(Properties prop) throws Exception {
		tf = LayerFactory.initialiseFactory(TableFactory.PROP_TABLEFACTORY, DEFAULT_TABLE_FACTORY, prop, TableFactory.class);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.record.RecordFactory#getRecordManger(java.lang.String)
	 */
	public RecordManager getRecordManger(String tableName){
		return new DefaultRecordManager(this, tableName);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.record.RecordFactory#getCollectionManager(java.lang.String)
	 */
	public CollectionManager getCollectionManager(String tableName) {
		return new DefaultCollectionManager(this, tableName);
	}

	/**
	 * Returns the TableInfo for a table
	 * 
	 * @param tableName    The name of the table to return the TableInfo for
	 */
	public TableFactory findTableFactory(){
		return tf;
	}

}