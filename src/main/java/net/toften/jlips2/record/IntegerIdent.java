package net.toften.jlips2.record;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:21
 */
public class IntegerIdent extends Ident {

	private int id;

	/**
	 * 
	 * @param id
	 */
	public IntegerIdent(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
	
	public Object getValue() {
		return id;
	}

	public boolean isNumeric() {
		return true;
	}
}