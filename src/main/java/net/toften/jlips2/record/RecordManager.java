package net.toften.jlips2.record;

import net.toften.jlips2.Entity;


/**
 * @created 26-Sep-2005 21:57:39
 * @author thomaslarsen
 * @version 1.0
 */
public interface RecordManager extends Entity {

	/**
	 * Creates a new record
	 */
	public ID create();

	/**
	 * Finds a record
	 * 
	 * @param id    The primary key of the record
	 */
	public ID find(ID id);

	/**
	 * @return
	 */
	public boolean isDirty();

}