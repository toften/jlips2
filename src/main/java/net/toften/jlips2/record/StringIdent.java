package net.toften.jlips2.record;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:23
 */
public class StringIdent extends Ident {

	private String id;

	/**
	 * 
	 * @param id
	 */
	public StringIdent(String id){
		this.id = new String(id);
	}

	public String getId(){
		return id;
	}
	
	public Object getValue() {
		return id;
	}

	public boolean isNumeric() {
		return false;
	}
}