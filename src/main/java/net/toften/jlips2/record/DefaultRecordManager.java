package net.toften.jlips2.record;
import java.util.HashMap;
import java.util.Map;

import net.toften.jlips2.db.DbException;
import net.toften.jlips2.db.table.TableFactory;
import net.toften.jlips2.db.table.TableManager;

/**
 * @created 26-Sep-2005 21:57:38
 * @author thomaslarsen
 * @version 1.0
 */
public class DefaultRecordManager implements RecordManager {

	private TableFactory 		tf;
	private String 				tableName;

	private ID 					primarykey 		= null;
	private Map<String, Object> currentValues 	= new HashMap<String, Object>();
	private Map<String, Object> newValues 		= new HashMap<String, Object>();
	
	/**
	 * @param rf
	 * @param tableName
	 */
	public DefaultRecordManager(DefaultRecordFactory rf, String tableName) {
		this.tableName = tableName;
		
		tf = rf.findTableFactory();
		
		// Test to see if a TableManager exists		
		if (tf.getTableManager(tableName) == null)
			throw new IllegalStateException("Can not find TableManager for table: " + tableName);

	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.record.RecordManager#create()
	 */
	public ID create(){
		TableManager tm = tf.getTableManager(tableName);

		try {
			primarykey = tm.insertRecord();
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return primarykey;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.record.RecordManager#find(net.toften.jlips2.record.ID)
	 */
	public ID find(ID id){
		TableManager tm = tf.getTableManager(tableName);
		
		try {
			primarykey = tm.getRecord(id, currentValues);
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return primarykey;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.Entity#remove()
	 */
	public void remove() {
		if (primarykey == null)
			throw new IllegalStateException("Record is not existing");
		
		TableManager tm = tf.getTableManager(tableName);

		try {
			tm.deleteRecord(primarykey);
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		primarykey = null;
		currentValues = null;
		newValues = null;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.Entity#commit()
	 */
	public void commit() {
		if (primarykey == null)
			throw new IllegalStateException("Record is not existing");
		
		if (isDirty()) {
			TableManager tm = tf.getTableManager(tableName);
	
			try {
				tm.updateRecord(primarykey, newValues);
			} catch (DbException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			currentValues.putAll(newValues);
			newValues = new HashMap<String, Object>();
		}
		
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.Entity#rollback()
	 */
	public void rollback() {
		if (primarykey == null)
			throw new IllegalStateException("Record is not existing");
		
		newValues = new HashMap<String, Object>();
		
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.Entity#set(java.lang.String, java.lang.Object)
	 */
	public void set(String fieldName, Object value){
		if (primarykey == null)
			throw new IllegalStateException("Record is not existing");
		
		newValues.put(fieldName, value);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.Entity#get(java.lang.String)
	 */
	public Object get(String fieldName){
		if (primarykey == null)
			throw new IllegalStateException("Record is not existing");
		
		Object returnValue = newValues.get(fieldName);
		
		if (returnValue == null) {
			returnValue = currentValues.get(fieldName);
		}
		
		return returnValue;
	}
	
	/* (non-Javadoc)
	 * @see net.toften.jlips2.record.RecordManager#isDirty()
	 */
	public boolean isDirty() {
		if (primarykey == null)
			throw new IllegalStateException("Record is not existing");
		
		return !newValues.isEmpty();
	}

}