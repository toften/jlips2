package net.toften.jlips2.record;

import java.util.Collection;

public interface CollectionManager {
	public Collection<ID> find(String relatedColumn, Object value); 
}
