package net.toften.jlips2.record;

/**
 * Identifies a single record
 * @created 25-Sep-2005 16:54:33
 * @author thomaslarsen
 * @version 1.0
 */
public abstract class Ident implements ID {

	/**
	 * 
	 * @param id
	 */
	public static final ID newIntegerId(int id){
		return new IntegerIdent(id);
	}

	/**
	 * 
	 * @param id
	 */
	public static final ID newStringId(String id){
		return new StringIdent(id);
	}
	
	public abstract Object getValue();
	
	public abstract boolean isNumeric();

}