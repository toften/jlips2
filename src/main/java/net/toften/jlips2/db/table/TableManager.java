package net.toften.jlips2.db.table;
import java.util.Collection;
import java.util.Map;

import net.toften.jlips2.db.DbException;
import net.toften.jlips2.record.ID;

/**
 * The responsibility of the TableManager is to represent a table in the database.
 * 
 * Each table in a database will have a TableManager. The TableManager is
 * responsible for providing life-cycle management methods for the upper layers of
 * jlips, most significantly to the RecordManager
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:23
 */
public interface TableManager {

	/**
	 * Must insert a new record in the table
	 * A new primary key must be generated and returned
	 * 
	 * @return The newly generated primary key
	 * @throws DbException Any exception must be rethrown as a
	 * DbException
	 */
	public ID insertRecord() throws DbException;

	/**
	 * Must update the values of the fields specified in the values
	 * Map. Only the fields in this map with values other than null
	 * have to be updated.
	 * 
	 * If an unknown field is specified in the Map, it must be ignored.
	 * 
	 * @param id The primary key of the record that must be updated
	 * @param values Map that contains the field names and values to be
	 * updated
	 * @throws DbException
	 */
	public void updateRecord(ID id, Map<String, Object> values) throws DbException;

	/**
	 * Must delete a record in the table
	 * 
	 * @param id The primary key of the record to delete
	 * @throws DbException
	 */
	public void deleteRecord(ID id) throws DbException;

	/**
	 * Must return a record from the table
	 * 
	 * If the values Map is defined, then the current values will be
	 * added to the Map passed in
	 * 
	 * @param id
	 * @param values
	 * @return
	 * @throws DbException
	 */
	public ID getRecord(ID id, Map<String, Object> values) throws DbException;
	
	public Collection<ID> getAllRecords() throws DbException;
	
	public Collection<ID> getSearch(Map<String, Object> values) throws DbException;

}