package net.toften.jlips2.db.table;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.toften.jlips2.db.DbException;
import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.db.data.primarykey.PrimarykeyFactory;
import net.toften.jlips2.db.data.statement.StatementFactory;
import net.toften.jlips2.db.data.statement.StatementManager;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.ColumnInfo;

import org.apache.log4j.Logger;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:20
 */
public class DbTableManager implements TableManager {
	
	private TableInfo tableInfo;
	
	private TableFactory tf;
	
	private StatementFactory sf;

	private PrimarykeyFactory pf;
	
	private static Logger dbLogger = Logger.getLogger("net.toften.jlips2.db");
	
	/**
	 * @param tf
	 * @param tableInfo
	 */
	public DbTableManager(DbTableFactory tf, TableInfo tableInfo) {
		this.tableInfo = tableInfo;
		this.tf = tf;
		
		sf = tf.findStatementFactory();
		pf = tf.findPrimarykeyFactory();
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.table.TableManager#insertRecord()
	 */
	public ID insertRecord() throws DbException {
		StatementManager sm = sf.getStatementManager(tableInfo);
		PrimaryKeyManager pm = pf.getPrimarykeyManager(tableInfo);
		
		sm.setPrimarykey(pm);
		
		try {
			pm = sm.doInsert();
		} catch (SQLException e) {
			throw new DbException("Exception in insertRecord", e);
		} finally {
			try {
				sm.getConnectionManager().release();
			} catch (SQLException e) {
				throw new DbException("Exception in release connection", e);
			}
		}
		
		return pm;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.table.TableManager#updateRecord(net.toften.jlips2.record.ID, java.util.Map)
	 */
	public void updateRecord(ID id, Map<String, Object> fields) throws DbException {
		StatementManager sm = sf.getStatementManager(tableInfo);
		PrimaryKeyManager pm = pf.getPrimarykeyManager(tableInfo, id);
		
		sm.setPrimarykey(pm);
		
		for (String columnName : fields.keySet()) {
			ColumnInfo columnInfo = tableInfo.getColumnInfo(columnName);
			if (columnInfo != null) {
				Object value = fields.get(columnName);
				
				sm.addEqualExpression(value, columnInfo);
			} else {
				dbLogger.warn("Attemted to change unknown column: " + columnName + " in table: " + tableInfo.getName());
			}
		}

		try {
			sm.doUpdate();
		} catch (SQLException e) {
			throw new DbException("Exception in updateRecord", e);
		} finally {
			try {
				sm.getConnectionManager().release();
			} catch (SQLException e) {
				throw new DbException("Exception in release connection", e);
			}
		}
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.table.TableManager#deleteRecord(net.toften.jlips2.record.ID)
	 */
	public void deleteRecord(ID id) throws DbException {
		StatementManager sm = sf.getStatementManager(tableInfo);
		PrimaryKeyManager pm = pf.getPrimarykeyManager(tableInfo, id);
		
		sm.setPrimarykey(pm);
		
		try {
			sm.doDelete();
			
		} catch (SQLException e) {
			throw new DbException("Exception in deleteRecord", e);
		} finally {
			try {
				sm.getConnectionManager().release();
			} catch (SQLException e) {
				throw new DbException("Exception in release connection", e);
			}
		}
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.table.TableManager#getRecord(net.toften.jlips2.record.ID)
	 */
	public ID getRecord(ID id, Map<String,Object> fields) throws DbException {
		StatementManager sm = sf.getStatementManager(tableInfo);
		PrimaryKeyManager pm = pf.getPrimarykeyManager(tableInfo, id);
		
		sm.setPrimarykey(pm);
		
		ResultSet rs;
		
		try {
			rs = sm.doSelect();
			
		} catch (SQLException e) {
			throw new DbException("Exception in getRecord", e);
		}
		
		if (fields != null) {
			try {
				while (rs.next()) {
					for (ColumnInfo col : tableInfo.columns) {
						String fieldName = col.getName();
						Object value = rs.getObject(fieldName);
						
						if (value != null)
							fields.put(fieldName.toLowerCase(), value);
					}
				}
			} catch (SQLException e) {
				throw new DbException("Exception in getRecord", e);
			}
		}
		
		try {
			sm.getConnectionManager().release();
		} catch (SQLException e) {
			throw new DbException("Exception in release connection", e);
		}

		return pm;
		
	}
	
	/**
	 * Will return the TableInfo object for the TableManager
	 * 
	 * @return
	 */
	public TableInfo getTableInfo() {
		return tableInfo;
	}

	public Collection<ID> getAllRecords() throws DbException {
		StatementManager sm = sf.getStatementManager(tableInfo);
		
		sm.addColumn(tableInfo.primarykeyColumn);

		ResultSet rs = null;
		
		try {
			rs = sm.doSelect();
		} catch (SQLException e) {
			throw new DbException("Exception in selectRecord", e);
		}
		
		return new ResultSetWrapper(rs);
	}

	/**
	 * This method returns a Collection searching the value of a foreign
	 * field.
	 * 
	 * The method can be used for 1 to many relationships.
	 * 
	 * The method will wrap the resulting ResultSet and will not release it
	 * until the Collection is garbage collected.
	 * 
	 * The method will take a parameter that is the foreign key in the table and
	 * a parameter that is the primary key value in the foreign table.
	 * It will return a Collection of primary keys.
	 * 
	 * 				+---------+                +----------+
	 * 				|  A      |                |    B     |
	 * 				+---------+ 1            * +----------+
	 * 				| id (PK) |--------------->| id  (PK) |
	 * 				+---------+                | aid (FK) |
	 * 				                           +----------+
	 *                            
	 * In the above diagram, this TableHandler is the handler for table B. We are 
	 * after a collection of B records that are related to A in a 1 to many
	 * relationship.
	 * The parameters that must be passed in are:
	 * <ul>
	 * <li>"aid" as the <code>foreignKeyFieldName</code></li>
	 * <li>The primary key for the related A record as <code>id</code></li>
	 * </ul>
	 * The method will return a Collection of B records that are related to 
	 * the passed in A record.
	 * 
	 * @param foreignKeyFieldName 
	 * @param id
	 * @return
	 * @throws DbException
	 */
	public Collection<ID> getSearch(String foreignKeyFieldName, ID id) throws DbException {
		Object relatedPkValue = null;
		TableInfo relatedTableInfo;
		
		// Check the passed in id
		if (id instanceof PrimaryKeyManager) {
			PrimaryKeyManager pm 	= (PrimaryKeyManager)id;
			relatedPkValue 			= pm.getValue();
			relatedTableInfo 		= pm.getPrimarykeyColumn().theTable; 
		} else {
			throw new IllegalStateException("Passed in primary key id not defined");
		}
		
		// Search the records from the database
		StatementManager sm = sf.getStatementManager(tableInfo);
		
		sm.addColumn(tableInfo.primarykeyColumn); // Only return the primary key
		
		ColumnInfo columnInfo = tableInfo.getColumnInfo(foreignKeyFieldName);
		if (columnInfo != null) {
			sm.addEqualExpression(relatedPkValue, columnInfo);
		} else {
			dbLogger.warn("Attemted to search using unknown column: " + foreignKeyFieldName + " in table: " + tableInfo.getName());
		}

		ResultSet rs = null;
		
		try {
			rs = sm.doSelect();
		} catch (SQLException e) {
			throw new DbException("Exception in selectRecord", e);
		}
		
		return new ResultSetWrapper(rs, foreignKeyFieldName, relatedTableInfo, relatedPkValue);
	}
	
	class ResultSetWrapper implements Collection<ID> {
		private ResultSet rs;
		private Object relatedPkValue = null;
		private TableInfo relatedTableInfo = null;
		private String relatedFieldName = null;
		
		ResultSetWrapper(ResultSet rs, String relatedFieldName, TableInfo relatedTableInfo, Object relatedPkValue) {
			this.rs = rs;
			this.relatedPkValue = relatedPkValue;
			this.relatedTableInfo = relatedTableInfo;
			this.relatedFieldName = relatedFieldName;
		}

		public ResultSetWrapper(ResultSet rs) {
			this.rs = rs;
		}

		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}

		public boolean isEmpty() {
			boolean isEmpty = false;
			
			try {
				isEmpty = rs.isAfterLast() && rs.isBeforeFirst();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return isEmpty;
		}

		public boolean contains(Object o) {
			// TODO Auto-generated method stub
			return false;
		}

		public Iterator<ID> iterator() {
			return new ResultSetIterator(rs);
		}

		public Object[] toArray() {
			// TODO Auto-generated method stub
			return null;
		}

		public <T> T[] toArray(T[] a) {
			// TODO Auto-generated method stub
			return null;
		}

		/**
		 * This method will add a new record to the relationship.
		 * 
		 * It will set the value of the foreign key to the value
		 * of the related record in the 1 to many relationship
		 * 
		 * @param id
		 * @return
		 */
		public boolean add(ID id) {
			if (!(id instanceof PrimaryKeyManager))
				throw new IllegalStateException("Passed in primary key id not defined");
			
			PrimaryKeyManager pm = (PrimaryKeyManager)id;
			if (pm.getPrimarykeyColumn().theTable != tableInfo)
				throw new IllegalArgumentException("The passed in record is from a different table");
			
			// Find the record and update it
			Map<String, Object> fields = new HashMap<String, Object>();
			fields.put(relatedFieldName, relatedPkValue);
			try {
				updateRecord(id, fields);
			} catch (DbException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
		}

		public boolean remove(Object o) {
			// TODO Auto-generated method stub
			return false;
		}

		public boolean containsAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}

		public boolean addAll(Collection<? extends ID> c) {
			// TODO Auto-generated method stub
			return false;
		}

		public boolean removeAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}

		public boolean retainAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}

		public void clear() {
			// TODO Auto-generated method stub
			
		}
		
		class ResultSetIterator implements Iterator<ID> {
			ResultSet rs;
			int lastRow = 0;
			
			public ResultSetIterator(ResultSet rs) {
				this.rs = rs;
			}

			public boolean hasNext() {
				boolean hasNext = false;
				
				try {
					hasNext = rs.next();
					lastRow++;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return hasNext;
			}

			public ID next() {
				ID id = null;
				
				try {
					if (tableInfo.primarykeyColumn.isNumeric())
						id = Ident.newIntegerId(rs.getInt(1));
					else
						id = Ident.newStringId(rs.getString(1));
				} catch (Exception e) {
					// TODO: handle exception
				}

				return id;
			}

			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
		}
	}

}