package net.toften.jlips2.db.table;
import net.toften.jlips2.BaseFactory;

/**
 * The TableFactory creates TableManagers
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:23
 */
public interface TableFactory extends BaseFactory {

	String PROP_TABLEFACTORY = "net.toften.jlips2.db.table.TableFactory";

	/**
	 * Returns a manager for a specified table.
	 * 
	 * The table is specified by its name as a String.
	 * If the table is not managed by the factory, null is returned
	 * 
	 * @param tableName    The name of the table to return a TableManager for
	 */
	public TableManager getTableManager(String tableName);

}