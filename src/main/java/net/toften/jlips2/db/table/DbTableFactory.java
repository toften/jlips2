package net.toften.jlips2.db.table;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.jlips2.Entity;
import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.db.DbException;
import net.toften.jlips2.db.data.connection.ConnectionFactory;
import net.toften.jlips2.db.data.primarykey.PrimarykeyFactory;
import net.toften.jlips2.db.data.statement.StatementFactory;
import net.toften.jlips2.entity.EntityInfo;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:20
 */
public class DbTableFactory extends LayerFactory implements TableFactory {

	public static final String DEFAULT_STATEMENT_FACTORY = "net.toften.jlips2.db.data.statement.Sql92StatementFactory";
	public static final String DEFAULT_PRIMARYKEY_FACTORY = "net.toften.jlips2.db.data.primarykey.DbPrimarykeyFactory";
	
	private StatementFactory sf;
	private PrimarykeyFactory pf;

	/**
	 * This Map maps between a tablename and a TableManager object read
	 * from the database
	 * 
	 * The key is the name of the table in lower case
	 */
	private Map<String, DbTableManager> tmMap = new HashMap<String, DbTableManager>();

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.table.TableFactory#getTableManager(java.lang.String)
	 */
	public TableManager getTableManager(String tableName){
		TableManager returnValue;
		
		if ((returnValue = tmMap.get(tableName.toLowerCase())) == null)
			dbLogger.warn("Request for non-existing TableManager " + tableName + "made");
		
		return returnValue;
	}

	/**
	 * Initialises the table factory
	 * 
	 * 
	 * @param prop    prop
	 */
	@SuppressWarnings("unchecked")
	public boolean init(Properties prop) throws Exception {
		boolean returnValue = false;
		
		sf = LayerFactory.initialiseFactory(
				StatementFactory.PROP_STATEMENTFACTORY, 
				DEFAULT_STATEMENT_FACTORY, 
				prop, 
				StatementFactory.class);
		
		pf = LayerFactory.initialiseFactory(
				PrimarykeyFactory.PROP_PRIMARYKEYFACTORY, 
				DEFAULT_PRIMARYKEY_FACTORY, 
				prop, 
				PrimarykeyFactory.class);

		/*
		 * A TableManager object will be created for each table
		 */		
		Map<String, TableInfo> tableinfoMap = 
			(Map<String, TableInfo>) prop.get(ConnectionFactory.PROP_TABLEINFO);

		Map<Class<? extends Entity>, String> entityMap = 
			(Map<Class<? extends Entity>, String>)prop.get(EntityInfo.PROP_ENTITYMAP);

		if (tableinfoMap != null && entityMap != null) {
			// Get all the tables that an entity interface has been defined for
			String[] tableNames = new String[entityMap.size()];
			entityMap.values().toArray(tableNames);
			
			/* 
			 * Run through the tables defined in the entityMap and find a TableInfo
			 * for each of them
			 */
			for (String tableName: tableNames) {
				TableInfo tableInfo = tableinfoMap.get(tableName.toLowerCase());
				
				if (tableInfo != null) {
					DbTableManager tm = new DbTableManager(this, tableInfo);
					tmMap.put(tableName.toLowerCase(), tm); // Lower case the table name
					dbLogger.debug("Table " + tableName + " is mapped");
				} else {
					dbLogger.error("No TableInfo object for table:" + tableName);
					
					throw new DbException("TableInfo for table " + tableName + " not found.");
				}
			}
			
			returnValue = true;
		}
		
		if (entityMap != null && tableinfoMap == null) {
			dbLogger.error("Entity map found, but no tables found");
			throw new DbException("Entity map found, but no tables found");
		}
		
		return returnValue;
	}

	/**
	 * 
	 * @param tableName
	 */
	public StatementFactory findStatementFactory(){
		return sf;
	}

	/**
	 * 
	 * @param tableName
	 */
	public PrimarykeyFactory findPrimarykeyFactory(){
		return pf;
	}
	
	public TableInfo findTableInfo(String tableName) {
		TableInfo returnValue = null;
		
		DbTableManager tm = tmMap.get(tableName.toLowerCase());
		if (tm != null)
			returnValue = tm.getTableInfo();
		
		return returnValue;
		
	}

}