package net.toften.jlips2.db;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author thomas
 *
 */
public interface SqlExecutedListener {
	/**
	 * @param s
	 * @throws SQLException
	 */
	public void statementReturned(Statement s) throws SQLException;
}
