package net.toften.jlips2.db.data.statement;


import java.util.Set;

import net.toften.jlips2.db.data.connection.ConnectionManager;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.ColumnInfo;

import org.apache.log4j.Logger;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:22
 */
public class Sql92StatementManager extends SqlStatementManager implements StatementManager {
	private ConnectionManager cm = null;
	
	private Sql92StatementFactory sf = null;
	protected TableInfo tableInfo = null;
	/**
	 * Creates an Sql92StatementManager
	 * 
	 * Will add the specified table to the collected tabels
	 * 
	 * @param tableName
	 * 
	 * @see SqlStatementManager#addTable(String)
	 */
	public Sql92StatementManager(Sql92StatementFactory sf, TableInfo tableInfo){
		this.sf = sf;
		this.tableInfo = tableInfo;
		
		addTable(tableInfo);
	}
	
	/**
	 * @param s
	 */
	private void addWhereStatment(StringBuffer s) {
		// Add where statement from primary key and expressions
		if (primarykeyManager != null || !expressions.isEmpty()) {
			s.append(" where ");
			
			addPrimarykeyStatement(s);
			
			if (!expressions.isEmpty()) {
				if (primarykeyManager != null) {
					s.append(" and ");
				}
				s.append(linkList(expressions, "=", " and "));
			}
		}
	}

	/**
	 * @param s
	 */
	private void addPrimarykeyStatement(StringBuffer s) {
		if (primarykeyManager != null) {
			s.append(primarykeyManager.getPrimarykeyName() + "=" + primarykeyManager.getStatementString());
		}
	}
	
	private void validatePrimarykeyIsSelectable() {
		if (primarykeyManager != null)
			if (primarykeyManager.getStatementString() == null)
				throw new IllegalStateException("Primary key is not selectable");
	}
	
	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.SqlStatementManager#getConnectionManager()
	 */
	protected ConnectionManager createConnectionManager() {
		cm = sf.findConnectionFactory().getConnectionManager();
		return cm;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.SqlStatementManager#createSelectStatement()
	 */
	public String createSelectStatement() {
		validatePrimarykeyIsSelectable();
		
		StringBuffer s = new StringBuffer();
		
		s.append("select ");
		s.append(linkList(columns, ",", "*"));
		s.append(" from ");
		s.append(linkList(tables, ","));
		
		addWhereStatment(s);
		
		log.debug("Select statement created: " + s);
		
		return s.toString();
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.SqlStatementManager#createInsertStatement()
	 */
	public String createInsertStatement() {
		StringBuffer s = new StringBuffer();
		StringBuffer cols = new StringBuffer(), vals = new StringBuffer();
		
		s.append("insert ");

		if (primarykeyManager != null) {
			if (primarykeyManager.isAutoGenerate()) {
				if (expressions.isEmpty()) { 
					// Only add primary key and 'null' if there are no other expressions
					cols.append(primarykeyManager.getPrimarykeyName());
					vals.append("null");
				}
			} else {
				cols.append(primarykeyManager.getPrimarykeyName());
				vals.append(primarykeyManager.getStatementString());
			}
		} else
			log.warn("No primary key manager defined in SQL insert statement");
		
		if (!expressions.isEmpty()) {
			Set<ColumnInfo> colKeys = expressions.keySet();
			
			for (ColumnInfo col : colKeys) {
				if (cols.length() > 0) { // Values has been added
					cols.append(",");
				}
				cols.append(col.getName());
				
				if (vals.length() > 0) { // Values has been added
					vals.append(",");
				}
				vals.append(col.getStatementString(expressions.get(col).toString()));
			}
			
		}
		
		s.append("into ");
		s.append(linkList(tables, ","));
		
		s.append(" (");
		s.append(cols);
		s.append(") values (");
		s.append(vals);
		s.append(")");
		
		log.debug("Insert statement created: " + s);

		return s.toString();		
	}
	
	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.SqlStatementManager#createDeleteStatement()
	 */
	public String createDeleteStatement() {
		validatePrimarykeyIsSelectable();
		
		StringBuffer s = new StringBuffer();
		
		s.append("delete from ");
		s.append(linkList(tables, ","));
		
		if (primarykeyManager == null && expressions.isEmpty())
			log.warn("No primary key or expressions provided in SQL delete statement");
		
		addWhereStatment(s);
		
		Logger.getLogger("net.toften.jlips2.db").debug("Delete statement created: " + s);

		return s.toString();
	}
	
	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.SqlStatementManager#createUpdateStatement()
	 */
	public String createUpdateStatement() {
		validatePrimarykeyIsSelectable();
		
		StringBuffer s = new StringBuffer();
		
		s.append("update ");
		s.append(linkList(tables, ","));

		if (!expressions.isEmpty()) {
			s.append(" set ");
			s.append(linkList(expressions, "=", ","));
		} else
			log.debug("No values to update in SQL update statement");
		
		if (primarykeyManager != null) {
			s.append(" where ");
			addPrimarykeyStatement(s);
		} else
			log.warn("No primary key specified for SQL update statement");
		
		log.debug("Update statement created: " + s);

		return s.toString();		
	}

	public ConnectionManager getConnectionManager() {
		
		return cm;
	}

}