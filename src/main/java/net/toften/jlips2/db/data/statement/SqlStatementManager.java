package net.toften.jlips2.db.data.statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import net.toften.jlips2.db.SqlExecutedListener;
import net.toften.jlips2.db.data.connection.ConnectionManager;
import net.toften.jlips2.db.data.primarykey.PrimaryKeyManager;
import net.toften.jlips2.table.DatabaseEntity;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.ColumnInfo;

import org.apache.log4j.Logger;

/**
 * This abstract class can be used as baseclass for concrete StatementManager
 * classes.
 * 
 * It provides common functionality that would be needed by StatementManagers,
 * supporting them in:
 * <ul>
 * <li>Collecting SQL statement building blocks</li>
 * <li>Executing the SQL statements</li>
 * </ul>
 * 
 * The responsibility of the concrete StatementManagers are to assemble
 * the actual SQL statement from the collected building blocks
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 04-Oct-2005 16:58:18
 */
public abstract class SqlStatementManager implements StatementManager {
	protected static Logger log = Logger.getLogger("net.toften.jlips2.db");

	protected static Logger sqlLog = Logger.getLogger("net.toften.jlips2.db.sql");

	/**
	 * Set that contains the collected columns
	 * 
	 * The use of the columns depend of what type of statement that
	 * is to be executed
	 */
	protected Set<ColumnInfo> columns = new LinkedHashSet<ColumnInfo>();
	
	/**
	 * Set that contains the collected tables 
	 */
	protected Set<TableInfo> tables = new LinkedHashSet<TableInfo>();
	
	/**
	 * Map that contains the collected expressions 
	 */
	protected Map<ColumnInfo, Object> expressions = new LinkedHashMap<ColumnInfo, Object>();
	
	/**
	 * Contains the supplied PrimaryKeyManager
	 */
	protected PrimaryKeyManager primarykeyManager = null;

	/**
	 * Links strings in a list using a specified delimiter
	 * 
	 * No delimiter will be added after the last element
	 * 
	 * @param stringList List with strings to be linked
	 * @param delimiter delimiter between elements
	 * @return the linked list
	 */
	protected static String linkList(Collection<? extends DatabaseEntity> stringList, String delimiter) {
		String returnValue = "";
		
		if (!stringList.isEmpty()) {
			StringBuffer linkedList = new StringBuffer();
			
			for (DatabaseEntity element : stringList) {
				if (linkedList.length() > 0) {
					linkedList.append(delimiter);
				}
				
				linkedList.append(element.getName());
			}
			
			returnValue = linkedList.toString();
		}
		
		return returnValue;
	}
	
	protected static String linkList(Map<ColumnInfo, Object> ex, String sign, String del) {
		String returnValue = "";
		
		if (!ex.isEmpty()) {
			StringBuffer linkedList = new StringBuffer();
			Collection<ColumnInfo> cols = ex.keySet();
						
			for (ColumnInfo col : cols) {
				if (linkedList.length() > 0) {
					linkedList.append(del);
				}
				
				linkedList.append(col.getName());
				linkedList.append(sign);
				linkedList.append(col.getStatementString(ex.get(col).toString()));
			}
			
			returnValue = linkedList.toString();
		}
		
		return returnValue;		
		
	}
	
	/**
	 * Links elements in a list using a specified delimiter. If the list is empty
	 * the defaultValue is returned
	 * @param l List with elements to be linked
	 * @param del delimiter between elements
	 * @param defaultValue value to return if the list is empty
	 * @return the linked list
	 */
	protected static String linkList(Collection<? extends DatabaseEntity> l, String del, String defaultValue) {
		String returnValue;
		
		if (l.isEmpty())
			returnValue = defaultValue;
		else
			returnValue = linkList(l, del);
		
		return returnValue;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#setPrimarykey(net.toften.jlips2.db.data.primarykey.PrimaryKeyManager)
	 */
	public void setPrimarykey(PrimaryKeyManager newVal){
		primarykeyManager = newVal;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#addColumn(net.toften.jlips2.table.TableInfo.ColumnInfo)
	 */
	public void addColumn(ColumnInfo columnInfo) {
		columns.add(columnInfo);
		addTable(columnInfo.theTable);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#addTable(net.toften.jlips2.table.TableInfo)
	 */
	public void addTable(TableInfo tableInfo) {
		tables.add(tableInfo);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#addEqualExpression(java.lang.Object, net.toften.jlips2.table.TableInfo.ColumnInfo)
	 */
	public void addEqualExpression(Object value, ColumnInfo columnInfo) {
		expressions.put(columnInfo, value);
	}

	/**
	 * This method must return a ConnectionManager object
	 * 
	 * It is the responsibility of the concrete class to return an object
	 * that impelements the ConnectionManager interface
	 * 
	 * @return
	 */
	protected abstract ConnectionManager createConnectionManager();

	/**
	 * This method must return a propper formatted update SQL
	 * statement
	 * 
	 * @return
	 */
	protected abstract String createUpdateStatement();

	/**
	 * This method must return a propper formatted delete SQL
	 * statement
	 * 
	 * @return
	 */
	protected abstract String createDeleteStatement();

	/**
	 * This method must return a propper formatted insert SQL
	 * statement
	 * 
	 * @return
	 */
	protected abstract String createInsertStatement();

	/**
	 * This method must return a propper formatted select SQL
	 * statement
	 * 
	 * @return
	 */
	protected abstract String createSelectStatement();

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#doSql(java.lang.String)
	 */
	public ResultSet doSql(String statement) throws SQLException {
		return doSql(statement, null);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#doSql(java.lang.String, net.toften.jlips2.db.SqlExecutedListener)
	 */
	public ResultSet doSql(String statement, SqlExecutedListener l) throws SQLException {
		ResultSet returnValue = null;
		
		ConnectionManager cm = createConnectionManager();
		
		cm.setStatement(statement);
		
		Statement s = cm.execute();
		
		sqlLog.info(statement);
		
		if (l != null)
			l.statementReturned(s);
		
		returnValue = s.getResultSet();
		
		return returnValue;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#doInsert()
	 */
	public PrimaryKeyManager doInsert() throws SQLException {
		doSql(createInsertStatement(), primarykeyManager);
		
		return primarykeyManager;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#doUpdate()
	 */
	public ResultSet doUpdate() throws SQLException {
		return doSql(createUpdateStatement());
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#doDelete()
	 */
	public ResultSet doDelete() throws SQLException {
		return doSql(createDeleteStatement());
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementManager#doSelect()
	 */
	public ResultSet doSelect() throws SQLException {
		return doSql(createSelectStatement());
	}

}