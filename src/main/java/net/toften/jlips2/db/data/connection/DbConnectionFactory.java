package net.toften.jlips2.db.data.connection;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.ColumnInfo;
import net.toften.jlips2.table.TableInfo.ForeignKeyColumn;
import net.toften.jlips2.table.TableInfo.PrimarykeyColumn;


/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:19
 */
public class DbConnectionFactory extends LayerFactory implements ConnectionFactory {
	
	private List<Connection> activeConnections = new Vector<Connection>();

	private String driverClassName;
	private String url;
	private String username;
	private String password;
	private String schema;
	
	private int connectionCount = 0;

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.connection.ConnectionFactory#getConnectionManager()
	 */
	public ConnectionManager getConnectionManager(){
		return new DbConnectionManager(this);
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.BaseFactory#init(java.util.Properties)
	 */
	public boolean init(Properties prop) throws Exception {
		driverClassName 	= prop.getProperty(ConnectionFactory.PROP_DRIVERCLASSNAME, "");
		url 				= prop.getProperty(ConnectionFactory.PROP_URL);
		username 			= prop.getProperty(ConnectionFactory.PROP_USERNAME);
		password 			= prop.getProperty(ConnectionFactory.PROP_PASSWORD);
		schema 				= prop.getProperty(ConnectionFactory.PROP_SCHEMA);
		
		if (!driverClassName.equals(""))
			Class.forName(driverClassName);
		else
			dbLogger.warn("No database driver class specified = - must be loaded elsewhere");
		
		/*
		 * Build the TableInfo map
		 * 
		 * The map will be build from the metadata in the database. The map
		 * will be put back into the properties supplied to this method, so
		 * it can be used by other init methods in jlips
		 * 
		 * Key:		Table name (String)
		 * Value:	TableInfo object
		 */
		Map<String, TableInfo> tableList = new HashMap<String, TableInfo>();
		
		Connection c;
		ResultSet rs;
		c = getConnection();
		
		DatabaseMetaData dbmd = null;
		dbmd = c.getMetaData();
		
		/*
		 * Get the tables in the database
		 */
        rs = dbmd.getTables(null, schema, "%", null);

        while (rs.next()) {
        	String tableName = rs.getString("TABLE_NAME");
        	String tableType = rs.getString("TABLE_TYPE");
        	
        	// Create a TableInfo class for each table
        	if (tableType.toLowerCase().equals("table")) {
        		TableInfo ti = new TableInfo(tableName);
        		
        		tableList.put(tableName.toLowerCase(), ti);
            	dbLogger.info("Found table: " + tableName);
        	}
        }
        
        /*
         * Get the primary key and column information for the tables
         */
        for (TableInfo tableInfo : tableList.values()) {
        	String tableName = tableInfo.getName();

        	// Primary key
            rs = dbmd.getPrimaryKeys(null, schema, tableName);
            
            /*
             * If there are more than one primary key, this method will only return
             * the last in the list.
             * 
             * As the order of the primary keys are undefined, it might produce
             * undefined results if there are more than one primary key
             */
            boolean found = false;
            while (rs.next()) {
            	String primarykeyName = rs.getString("COLUMN_NAME");
            	dbLogger.info("Primary key for table " + tableName + " is: " + primarykeyName);
            	
            	PrimarykeyColumn pkci = tableInfo.new PrimarykeyColumn(primarykeyName);
            	
            	// Warn if more than one primary key is found
            	if (found == false)
            		found = true;
            	else
            		dbLogger.warn("More than one primary key column for the table: " + tableName);
            }
            
            if (found == false)
            	dbLogger.error("No primary key found for table: " + tableName);
		        
	        // Foreign keys
            rs = dbmd.getImportedKeys(null, schema, tableName);

            while (rs.next()) {
            	String fkName = rs.getString("FKCOLUMN_NAME");
            	String fkTableName = rs.getString("PKTABLE_NAME"); 
            	dbLogger.info("Foreign key for table " + tableName + " found: FTable: " + fkTableName + ", FColumn: " + fkName);
            	
            	TableInfo fkt = tableList.get(fkTableName);
            	ForeignKeyColumn fkci = tableInfo.new ForeignKeyColumn(fkName, fkt);
            }
	        
	        // Column info
            rs = dbmd.getColumns(null, schema, tableName, "%");

            while (rs.next()) {
            	String columnName = rs.getString("COLUMN_NAME");
            	dbLogger.info("Column for table: " + tableName + " found: " + columnName);
            	
            	ColumnInfo ci = tableInfo.new ColumnInfo(columnName);
                
                // Additional column info
                ci.columnSize = rs.getInt("COLUMN_SIZE");
                ci.sqlType = rs.getShort("DATA_TYPE");
            }
        }

        /*
         * Find the last value of the primary keys
         */
        TableInfo[] ti = new TableInfo[tableList.size()];
        tableList.values().toArray(ti);
        
        for (TableInfo tableInfo : ti) {
        	String primarykeyName = tableInfo.primarykeyColumn.getName();
        	String tableName = tableInfo.getName();
        	String sql = "select max(" + primarykeyName + ") from " + tableName;
        	
        	rs = c.createStatement().executeQuery(sql);
        	Object lastPk = null;
        	while (rs.next()) {
        		lastPk = rs.getObject(1);
        	}
        	
        	if (lastPk == null) {
        		tableInfo.primarykeyColumn.resetPrimarykeyValue();
        		dbLogger.warn("Primarykey value for " + tableName + "has been resat");
        	} else {
        		tableInfo.primarykeyColumn.lastPrimarykeyValue = lastPk;
        		dbLogger.info("Last primarykey value for " + tableName + ": " + lastPk);
        	}
        	
        }
        
        releaseConnection(c);
		
        /*
         * Put the created tableinfo map back into the properties
         */
        prop.put(ConnectionFactory.PROP_TABLEINFO, tableList);
        dbLogger.debug("Tablemap inserted into properties");
        
		return true;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.connection.ConnectionFactory#getConnection()
	 */
	public Connection getConnection() throws SQLException {
		Connection returnValue = null;
		
		returnValue = DriverManager.getConnection(url, username, password);
		activeConnections.add(returnValue);
		
		dbLogger.debug("Connection number " + ++connectionCount + " created");
		
		return returnValue;
	}
	
	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.connection.ConnectionFactory#releaseConnection(java.sql.Connection)
	 */
	public void releaseConnection(Connection c) throws IllegalStateException {
		int index = activeConnections.indexOf(c);
		
		if (index != -1) {
			activeConnections.remove(index);
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
			throw new IllegalArgumentException("Connection not found");
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.connection.ConnectionFactory#getNumberOfActiveConnections()
	 */
	public int getNumberOfActiveConnections() {
		// TODO Auto-generated method stub
		return activeConnections.size();
	}

}