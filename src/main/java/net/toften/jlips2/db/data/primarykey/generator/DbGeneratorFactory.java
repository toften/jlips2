package net.toften.jlips2.db.data.primarykey.generator;
import java.util.Properties;

import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.table.TableInfo;

/**
 * Implementation of the GeneratorFactory
 * 
 * This implementation will return a GeneratorManager implemented by the
 * {@link net.toften.jlips2.db.data.primarykey.generator.DbGeneratorManager} class
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 15-Oct-2005 23:58:02
 */
public class DbGeneratorFactory extends LayerFactory implements GeneratorFactory {

	/* (non-Javadoc)
	 * @see net.toften.jlips2.BaseFactory#init(java.util.Properties)
	 */
	public boolean init(Properties prop) throws Exception {
		dbLogger.info("DbGeneratorFactory initialised");
		
		return true;
	}

	/**
	 * Returns a primary key GeneratorManager for a specified table
	 * 
	 * @param tableInfo    The info of the table this is a primary key generator for
	 * 
	 * @throws IllegalArgumentException if no tableInfo is passed in
	 */
	public GeneratorManager getGeneratorManager(TableInfo tableInfo){
		if (tableInfo == null)
			throw new IllegalArgumentException("No table passed in");
		
		return new DbGeneratorManager(tableInfo);
	}

}