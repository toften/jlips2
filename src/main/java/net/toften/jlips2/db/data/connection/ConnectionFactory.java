package net.toften.jlips2.db.data.connection;

import java.sql.Connection;
import java.sql.SQLException;

import net.toften.jlips2.BaseFactory;

/**
 * The responsibility of the ConnectionFactory is to generate new
 * ConnectionManagers and to provide and manage the actual 
 * connections to the database
 * 
 * The actual connections to the database should only be given
 * to the ConnectionManagers
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:19
 */
public interface ConnectionFactory extends BaseFactory {

	public static final String PROP_CONNECTIONFACTORY = "net.toften.jlips2.db.data.connection.ConnectionFactory";
	public static final String PROP_DRIVERCLASSNAME = "ConnectionFactory.driverclassname";
	public static final String PROP_URL = "ConnectionFactory.url";
	public static final String PROP_USERNAME = "ConnectionFactory.username";
	public static final String PROP_PASSWORD = "ConnectionFactory.password";
	public static final String PROP_SCHEMA = "ConnectionFactory.schema";
	public static final String PROP_TABLEINFO = "net.toften.jlips2.table.TableInfo";

	/**
	 * This method must return a new ConnectionManager object
	 * 
	 * The ConnectionManager is created by the factory, and the 
	 * ConnectionManager can in turn use the database connection
	 * lifecycle methods provided by the ConnectionFactory
	 * 
	 * @return a new ConnectionManager
	 */
	public ConnectionManager getConnectionManager();
	
	/**
	 * Must return a new connection to the database
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException;
	
	/**
	 * This method is used to indicate to the ConnectionFactory, that
	 * the connection is no longer needed
	 * 
	 * The method should be used by the ConnectionManager to tell the
	 * ConnectionFactory that it no longer has a need for the connection
	 * 
	 * @see #getConnection()
	 * @see ConnectionManager#release()
	 * 
	 * @param c
	 */
	public void releaseConnection(Connection c);

	/**
	 * Must return the number of currently active connections 
	 * to the database
	 * 
	 * @return
	 */
	public int getNumberOfActiveConnections();
	
}