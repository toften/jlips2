package net.toften.jlips2.db.data.primarykey;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import net.toften.jlips2.db.data.primarykey.generator.GeneratorFactory;
import net.toften.jlips2.db.data.primarykey.generator.GeneratorManager;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.table.TableInfo;
import net.toften.jlips2.table.TableInfo.PrimarykeyColumn;

/**
 * This is a default implementation for a representation of a primary
 * key for a record in a database table.
 * 
 * 
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:20
 */
public class DbPrimarykeyManager implements PrimaryKeyManager {
	/**
	 * The primary key column in the TableInfo object of
	 * the table in which this is a primary key for a record
	 */
	private TableInfo.PrimarykeyColumn pk = null;

	/**
	 * The value of the primary key
	 * 
	 *  The initial value of the primary key will be null. The
	 *  primary key value will either be auto generated or generated
	 *  by the database.
	 *  
	 *  @see #isAutoGenerate()
	 *  @see #DbPrimarykeyManager(DbPrimarykeyFactory, TableInfo)
	 */
	private Object value = null;
	
	private static Logger dbLogger = Logger.getLogger("net.toften.jlips2.db");
	
	private void updateTableInfoPrimarykey() {
		pk.lastPrimarykeyValue = value;
		
	}
	
	/**
	 * This constructor will create a new primary key.
	 * 
	 * A value will be assigned if the table does not automatically
	 * generate a primary key. This is determined by using the method
	 * {@link #isAutoGenerate()}
	 *  
	 * @param pf
	 * @param tableInfo
	 * 
	 * @throws IllegalStateException if the primary key is not generated
	 * by the database and no {@link GeneratorFactory} is defined for the
	 * table
	 */
	public DbPrimarykeyManager(DbPrimarykeyFactory pf, TableInfo tableInfo) {
		this.pk = tableInfo.primarykeyColumn;
		
		if (!isAutoGenerate()) {
			GeneratorFactory gf = pf.findGeneratorFactory();
			
			if (gf == null)
				throw new IllegalStateException("Can not find GeneratorManager for table " + tableInfo.getName());
			
			GeneratorManager gm = gf.getGeneratorManager(tableInfo);
			
			this.value = gm.getNewPrimarykeyValue();
			updateTableInfoPrimarykey();
		}
	}
	
	/**
	 * This constructor will convert an {@link Ident} type into
	 * a primary key
	 * 
	 * The id must have a value and it must be of the same type as 
	 * the type of the primary key of the table
	 * 
	 * @param tableInfo
	 * @param id
	 * 
	 * @throws NullPointerException if the id provided is null
	 * @throws IllegalArgumentException if the id is of a different type
	 * than the one defined in the tableInfo
	 */
	public DbPrimarykeyManager(DbPrimarykeyFactory pf, TableInfo tableInfo, Ident id) {
		this.pk = tableInfo.primarykeyColumn;
		
		if (id == null)
			throw new NullPointerException("Ident not defined (is null)");
		
		if (id.isNumeric() != isNumeric())
			throw new IllegalArgumentException("Ident type is wrong");
		
		this.value = id.getValue();
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimaryKeyManager#isNumeric()
	 */
	public boolean isNumeric() {		
		return pk.isNumeric();
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimaryKeyManager#getPrimarykeyName()
	 */
	public String getPrimarykeyName(){
		return pk.getName();
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimaryKeyManager#isAutoGenerate()
	 */
	public boolean isAutoGenerate(){
		return pk.isAutoGenerate;
	}

	/**
	 * Must return the value of the primary key as a string
	 * 
	 * if the primary key value is not defined it must return
	 * null
	 * 
	 * @return primary key value as String or null
	 */
	public String toString() {
		String returnValue = null;
		
		if (value != null)
			returnValue = value.toString();
		
		return returnValue;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimaryKeyManager#getPrimarykeyColumn()
	 */
	public PrimarykeyColumn getPrimarykeyColumn() {
		// TODO Auto-generated method stub
		return pk;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimaryKeyManager#getValue()
	 */
	public Object getValue() {
		// TODO Auto-generated method stub
		return value;
	}

	/**
	 * This method is called after an <em>insert</em> statement has been executed
	 * 
	 * This allows the PrimayKeyManager to retreive any data
	 * from the statement, such as automatically generated keys
	 * 
	 * @param s the just executed statement
	 */
	public void statementReturned(Statement s) throws SQLException{
		if (isAutoGenerate()) {
			ResultSet rs = null;

			if (s.getConnection().getMetaData().supportsGetGeneratedKeys()) {
				rs = s.getGeneratedKeys();
			} else {
				if (s.getUpdateCount() == -1) {
					rs = s.getResultSet();
				} else {
					throw new IllegalArgumentException("No primary key generated");
				}
			}
			
			while (rs.next()) {
				value = rs.getObject(1); // Get primary key from first column
				dbLogger.info("Primarykey value " + value + " generated for table " + pk.theTable.getName());
				updateTableInfoPrimarykey();
			}
			
		}
		
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimaryKeyManager#getStatementString()
	 */
	public String getStatementString() {
		return pk.getStatementString(toString());
		
	}

}