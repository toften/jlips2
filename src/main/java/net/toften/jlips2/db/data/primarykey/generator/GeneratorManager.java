package net.toften.jlips2.db.data.primarykey.generator;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 15-Oct-2005 23:58:02
 * 
 * @see net.toften.jlips2.db.data.primarykey.generator.GeneratorFactory
 */
public interface GeneratorManager {

	/**
	 * Must return a new unique value to be used as a primary key for the table this
	 * GeneratorManager is a manager for
	 * 
	 * If the method is called successively it must <em>always</em> return 
	 * unique values, even if the values are not actually used as
	 * primary keys in the database
	 * 
	 * @return new unique primary key value
	 */
	public Object getNewPrimarykeyValue();

}