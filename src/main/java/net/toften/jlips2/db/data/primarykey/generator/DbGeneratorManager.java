package net.toften.jlips2.db.data.primarykey.generator;

import java.sql.Types;

import net.toften.jlips2.table.TableInfo;

/**
 * This primary key generator can automatically generate
 * new primary keys for new records in a table.
 * 
 * The generator will only generate keys for numeric primary keys:
 * <ul>
 * <li>Types.INTEGER</li>
 * <li>Types.BIGINT</li>
 * <li>Types.SMALLINT</li>
 * <li>Types.TINYINT</li>
 * </ul>
 * 
 * The generator will create a new primary key value by taking the
 * last used primary key value and add 1.
 * The generator will <em>not</em> check if the new primary key
 * has been used before. To ensure that already used keys are not
 * generated, the same primary key generator should always
 * be used to generate all primary keys in the table.
 * 
 * @author thomaslarsen
 * @version 1.0
 * @created 15-Oct-2005 23:58:02
 */
public class DbGeneratorManager implements GeneratorManager {

	/**
	 * The TableInfo object that this generator generates
	 * primary keys for
	 */
	private TableInfo tableInfo;
	
	/**
	 * Creates a new primary key generator for a specified
	 * table
	 * 
	 * @param tableInfo
	 */
	public DbGeneratorManager(TableInfo tableInfo) {
		this.tableInfo = tableInfo;
	}

	/**
	 * Returns a new unique value to be used as a primary key for the table this
	 * GeneratorManager is a manager for
	 */
	public Object getNewPrimarykeyValue(){
		Object newValue = null;
		Object oldValue = tableInfo.primarykeyColumn.getLastPrimarykeyValue();
		
		if (oldValue == null)
			throw new IllegalStateException("Current primarykey value not found");
		
		if (tableInfo.primarykeyColumn.isAutoGenerate)
			throw new IllegalStateException("Table automatically generates primary keys");
		
		switch (tableInfo.primarykeyColumn.sqlType) {
		case Types.INTEGER:
			newValue = new Integer(((Integer)oldValue) + 1);
			tableInfo.primarykeyColumn.lastPrimarykeyValue = newValue;
			break;

		case Types.BIGINT:
		case Types.SMALLINT:
		case Types.TINYINT:
			throw new RuntimeException("Not implemented yet");
			
		default:
			throw new IllegalStateException("Only Integer primary keys are allowed");
		}
		
		return newValue;
	}

}