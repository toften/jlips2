package net.toften.jlips2.db.data.primarykey;
import java.util.Properties;

import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.db.data.primarykey.generator.GeneratorFactory;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.record.Ident;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:20
 */
public class DbPrimarykeyFactory extends LayerFactory implements PrimarykeyFactory {

	private GeneratorFactory gf = null;
	
	/**
	 * 
	 */
	public static final String DEFAULT_GENERATOR_FACTORY = null;

	/* (non-Javadoc)
	 * @see net.toften.jlips2.BaseFactory#init(java.util.Properties)
	 */
	public boolean init(Properties prop) throws Exception{
		gf = LayerFactory.initialiseFactory(GeneratorFactory.PROP_GENERATORFACTORY, DEFAULT_GENERATOR_FACTORY, prop, GeneratorFactory.class);

		return true;
	}
	
	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimarykeyFactory#getPrimarykeyManager(net.toften.jlips2.table.TableInfo)
	 */
	public PrimaryKeyManager getPrimarykeyManager(TableInfo tableInfo){
		PrimaryKeyManager returnValue = new DbPrimarykeyManager(this, tableInfo);
		
		return returnValue;
	}

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.primarykey.PrimarykeyFactory#getPrimarykeyManager(net.toften.jlips2.table.TableInfo, net.toften.jlips2.record.ID)
	 */
	public PrimaryKeyManager getPrimarykeyManager(TableInfo tableInfo, ID id) {
		PrimaryKeyManager returnValue = null;
		
		if (id instanceof PrimaryKeyManager) {
			PrimaryKeyManager pk = (PrimaryKeyManager)id;
			
			/*
			 * The id provided is an initialised PrimaryKeyManager
			 * 
			 * The following checks are made:
			 * - It is the same table
			 */
			if (tableInfo != pk.getPrimarykeyColumn().theTable)
				throw new IllegalArgumentException("PrimarykeyManager for table " + pk.getPrimarykeyColumn().theTable.getName() + " found. " + tableInfo.getName() + " expected.");
			
			returnValue = pk;
		}
		else if (id instanceof Ident) {
			Ident value = (Ident)id;
			
			returnValue = new DbPrimarykeyManager(this, tableInfo, value);
		}
		
		return returnValue;
		
	}

	/**
	 * Finds a primary key GeneratorFactory for a given table
	 * 
	 * If the table does not have a GeneratorFactory associated with it it
	 * will return null
	 * 
	 * @return associated GeneratorFactory or null if none associated
	 */
	public GeneratorFactory findGeneratorFactory()  {
		return gf;
		
	}

}