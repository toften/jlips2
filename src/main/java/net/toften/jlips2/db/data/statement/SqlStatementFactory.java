package net.toften.jlips2.db.data.statement;
import java.util.Properties;

import net.toften.jlips2.LayerFactory;
import net.toften.jlips2.db.data.connection.ConnectionFactory;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:23
 */
public abstract class SqlStatementFactory extends LayerFactory {
	/**
	 * ConnctionFactory used for tables
	 */
	private ConnectionFactory cf;

	/**
	 * The default connection factory to use
	 */
	public static final String DEFAULT_CONNECTION_FACTORY = "net.toften.jlips2.db.data.connection.DbConnectionFactory";

	/* (non-Javadoc)
	 * @see net.toften.jlips2.BaseFactory#init(java.util.Properties)
	 */
	public boolean init(Properties prop) throws Exception {
		cf = LayerFactory.initialiseFactory(ConnectionFactory.PROP_CONNECTIONFACTORY, DEFAULT_CONNECTION_FACTORY, prop, ConnectionFactory.class);
		
		return true;
	}

	/**
	 * 
	 * 
	 * @param tableName
	 * @return
	 */
	public ConnectionFactory findConnectionFactory(){
		return cf;
	}

}