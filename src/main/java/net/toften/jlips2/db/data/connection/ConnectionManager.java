package net.toften.jlips2.db.data.connection;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:19
 */
public interface ConnectionManager {

	/**
	 * Sets the SQL statement that must be performed over the connection
	 */
	public void setStatement(String statement);

	/**
	 * Releases the connection
	 * 
	 * This method should be called by the user of the 
	 * ConnectionManager when the actual connection to the
	 * database is not needed anymore
	 * 
	 * @throws SQLException
	 */
	public void release() throws SQLException;

	/**
	 * Executes the statement!
	 * 
	 * This method must execute the SQL statement on the database
	 * and return the resulting Statement object
	 * 
	 * @return The result of the SQL statement
	 */
	public Statement execute() throws SQLException;

}