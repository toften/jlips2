package net.toften.jlips2.db.data.statement;
import net.toften.jlips2.BaseFactory;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:23
 */
public interface StatementFactory extends BaseFactory {

	public static final String PROP_STATEMENTFACTORY = "net.toften.jlips2.db.statement.StatementFactory";

	public StatementManager getStatementManager(TableInfo tableInfo);
	
}