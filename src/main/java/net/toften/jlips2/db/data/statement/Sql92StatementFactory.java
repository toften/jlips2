package net.toften.jlips2.db.data.statement;

import net.toften.jlips2.table.TableInfo;


/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:22
 */
public class Sql92StatementFactory extends SqlStatementFactory implements StatementFactory {

	/* (non-Javadoc)
	 * @see net.toften.jlips2.db.data.statement.StatementFactory#getStatementManager(java.lang.String)
	 */
	public StatementManager getStatementManager(TableInfo tableInfo){
		return new Sql92StatementManager(this, tableInfo);
		
	}

}