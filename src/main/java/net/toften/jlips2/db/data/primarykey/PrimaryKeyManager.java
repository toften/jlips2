package net.toften.jlips2.db.data.primarykey;
import java.sql.Statement;

import net.toften.jlips2.db.SqlExecutedListener;
import net.toften.jlips2.record.ID;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 03-Oct-2005 20:29:22
 */
public interface PrimaryKeyManager extends ID, SqlExecutedListener {

	/**
	 * Must return the primary key value as a string with 
	 * the appropriate qoutes depending
	 * on the type of the primary key
	 * 
	 * @return value as String with qoutes
	 * 
	 * @see #isNumeric()
	 */
	public String getStatementString();
	
	/**
	 * Will return the name of the primary key as it is defined
	 * in the database
	 * 
	 * @return the name of the primary key column
	 */
	public String getPrimarykeyName();
	
	/**
	 * Detemines if the primary key is numeric
	 * 
	 * This method must only return false if the primary key is a string
	 * 
	 * @return true if the type of the primary key is numeric
	 */
	public boolean isNumeric();
	
	/**
	 * Detemines if the table will automatically generate a primary key
	 * 
	 * @return true if the table automatically generates a primary key
	 * 
	 * @see #processStatementAfterInsert(Statement)
	 */
	public boolean isAutoGenerate();

	/**
	 * Must return the PrimarykeyColumn associated with the PrimaryKeyManager
	 * 
	 * @return PrimarykeyColumn object
	 */
	public TableInfo.PrimarykeyColumn getPrimarykeyColumn();

	/**
	 * Must return the value of the primary key as an Object
	 * 
	 * @return primary key value
	 */
	public Object getValue();

}