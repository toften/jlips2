package net.toften.jlips2.db.data.primarykey.generator;
import net.toften.jlips2.BaseFactory;
import net.toften.jlips2.table.TableInfo;

/**
 * @author thomaslarsen
 * @version 1.0
 * @created 15-Oct-2005 23:58:02
 */
public interface GeneratorFactory extends BaseFactory {

	public static final String PROP_GENERATORFACTORY = "net.toften.jlips2.db.data.primarykey.generator.GeneratorFactory";

	/**
	 * Returns a primary key GeneratorManager for a specified table
	 * 
	 * The returned primary key generator must be the same <em>every</em> time
	 * this method is invoked for a specific table
	 * 
	 * @param tableName    The name of the table this is a primary key generator for
	 */
	public GeneratorManager getGeneratorManager(TableInfo tableInfo);

}