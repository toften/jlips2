package net.toften.jlips2;

import java.util.Properties;

/**
 * Base interface for all factories in jLips2
 * 
 * @created 26-Sep-2005 21:16:07
 * @author thomaslarsen
 * @version 1.0
 */
public interface BaseFactory {

	/**
	 * This method will be called on all factories when they are
	 * instantiated.
	 * 
	 * This gives the factory the option to perform any initialisation
	 * that might be needed at that time
	 * 
	 * @param prop  Properties object passed in to jLips
	 * 				by the application during this initialisation
	 * 				sequence
	 */
	public boolean init(Properties prop) throws Exception;

}