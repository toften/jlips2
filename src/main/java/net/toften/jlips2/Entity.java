package net.toften.jlips2;

/**
 * @created 25-Sep-2005 16:34:21
 * @author thomaslarsen
 * @version 1.0
 */
public interface Entity {

	public void remove();

	/**
	 * 
	 * @param column    column
	 */
	public Object get(String column);

	/**
	 * 
	 * @param column    column
	 * @param value    The value to change the comun to
	 */
	public void set(String column, Object value);
	
	public void commit();
	
	public void rollback();

}